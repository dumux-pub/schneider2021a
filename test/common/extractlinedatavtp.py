#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import argparse
import sys
import numpy as np
from vtk import vtkXMLPolyDataReader
from vtk.util.numpy_support import vtk_to_numpy

# parse arguments
parser = argparse.ArgumentParser(
  prog='\033[1m\033[94m' + 'python' + '\033[0m' + ' ' + sys.argv[0],
  description='Averaging the outputs using control volumes.'
)
parser.add_argument('-f', '--file', nargs='+', required=True, help="vtp file to be processed")
parser.add_argument('-o', '--outputDirectory', default='', help="Directory to which the .csv files are written")
parser.add_argument('-of', '--outFile', nargs="+", default=['velocityValues'], help="Basename of the written csv file")
parser.add_argument('-p1', '--point1', type=float, nargs=3, required=True, help="Coordinates of the first point (in 3D)")
parser.add_argument('-p2', '--point2', type=float, nargs=3, required=True, help="Coordinates of the second point (in 3D)")
parser.add_argument('-v', '--verbosity', default=False, help='Verbosity of the output. True = print progress. False = print data columns')
args = vars(parser.parse_args())

## User Parameters
vtuFilename=args['file'][0]
verbose = args['verbosity']
p1 = args['point1']
p2 = args['point2']

# Read vtp data of PNM
reader = vtkXMLPolyDataReader()
reader.SetFileName(vtuFilename)
reader.Update()
polydata = reader.GetOutput()

points = vtk_to_numpy(polydata.GetPoints().GetData())

pointData = polydata.GetPointData()
v = vtk_to_numpy(pointData.GetArray("v"))
v = np.reshape(v, (len(points),1))

coords = []
velocityValues = []

def pointIsOnLine(point):
    if( np.abs(point[1]-p1[1]) < 1e-6 and p1[0] < point[0] + 1e-6 and p2[0] > point[0] - 1e-6 ):
        return True
    return False

for pIdx in range(0,len(points)):
    if pointIsOnLine(points[pIdx]):
        coords.append(points[pIdx])
        velocityValues.append(v[pIdx][0])

coords = np.reshape(coords, (len(coords),3))
velocityValues = np.reshape(velocityValues, (len(velocityValues),1))

#export
outDir = args['outputDirectory']
np.savetxt(outDir+args['outFile'][0]+".csv", np.column_stack((coords,velocityValues)), delimiter=",")
