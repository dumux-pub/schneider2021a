add_input_file_links()

dune_add_test(NAME test_stokes1p_vertical
              LABELS navierstokes
              SOURCES main.cc
              CMAKE_GUARD HAVE_UMFPACK
              COMMAND ${dumux_INCLUDE_DIRS}/bin/testing/runtest.py
              CMD_ARGS       --script fuzzy
                             --files ${CMAKE_SOURCE_DIR}/test/references/test_stokes1p_vertical-reference.vtu
                                     ${CMAKE_CURRENT_BINARY_DIR}/test_stokes1p_vertical-00035.vtu
                             --command "${CMAKE_CURRENT_BINARY_DIR}/test_stokes1p_vertical params.input
                             -Problem.Name test_stokes1p_vertical")

dune_add_test(NAME test_stokes1pni_vertical
              LABELS navierstokes
              SOURCES main.cc
              COMPILE_DEFINITIONS NONISOTHERMAL=1
              CMAKE_GUARD HAVE_UMFPACK
              COMMAND ${dumux_INCLUDE_DIRS}/bin/testing/runtest.py
              CMD_ARGS       --script fuzzy
                             --files ${CMAKE_SOURCE_DIR}/test/references/test_stokes1pni_vertical-reference.vtu
                                     ${CMAKE_CURRENT_BINARY_DIR}/test_stokes1pni_vertical-00035.vtu
                             --command "${CMAKE_CURRENT_BINARY_DIR}/test_stokes1pni_vertical params_nonisothermal.input
                             -Problem.Name test_stokes1pni_vertical")

  