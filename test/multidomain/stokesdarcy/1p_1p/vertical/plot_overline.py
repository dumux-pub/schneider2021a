#!/usr/bin/env python3
import argparse
import sys
import os
import numpy as np

try:
    commonPath = os.path.abspath(__file__)
    commonPath = commonPath.rsplit("build-cmake")[0]
    commonPath = os.path.join(commonPath, "test")
    commonPath = os.path.join(commonPath, "common")
    sys.path.append(commonPath)
    from mplconfig import plt
except Exception:
    sys.exit("Could not import mplconfig")

# parse arguments
parser = argparse.ArgumentParser(
  description='Plot the y-velocity from the obtained results.'
)
parser.add_argument('-f', '--file',
                    required=True,
                    help="csv file")
parser.add_argument('-l', '--label',
                    required=True,
                    help="label for plot")
parser.add_argument('-xlim', '--x_axis_limit',
                    type=float, nargs=2, default=None,
                    help="two values specifying the range of x-axis")
parser.add_argument('-o', '--outputName',
                    type=str, required=True,
                    help="the output name of the file")

args = vars(parser.parse_args())

boxData = np.loadtxt(args["file"], delimiter=",")

idx_x_box = 0
idx_vy_box = 3

plt.figure()
x = boxData[:, idx_x_box]
y = boxData[:, idx_vy_box]

extract = np.ones(len(x), dtype=bool)
if args["x_axis_limit"] is not None:
    xlim = args["x_axis_limit"]
    xmin = xlim[0]
    extract = x > xmin

x = x[extract]
y = y[extract]
    
plt.plot(x, y, label=args["label"], drawstyle="steps-post")
plt.ylabel("$v_y$ [m/s]")
plt.xlabel("x [m]")

plt.legend()

current_values = plt.gca().get_yticks()
plt.gca().set_yticklabels(['{:,.1e}'.format(x) for x in current_values])

plt.savefig(args["outputName"], bbox_inches="tight")
