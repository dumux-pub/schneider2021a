import argparse
import random
import numpy as np
parser = argparse.ArgumentParser()
parser.add_argument('-dx', '--deltaX', help='dx', required=True)
parser.add_argument('-dy', '--deltaY', help='dy', required=True)
parser.add_argument('-o', '--output', help='The output grid file', required=True)
args = vars(parser.parse_args())

dx = float(args["deltaX"])
dy = float(args["deltaY"])
outputname = args["output"] 
scaling = 0.0

outputfile = open(outputname, 'w')

# parse input file 
vertices = []
volumesCube = []

numEleX = int(0.8/dx + 1.1)
numEleY = int(1.2/dy + 1.1)

numDofX = numEleX + 1
numDofY = numEleY + 1


print("Deltax" + str(dx))

for i in range(0, numDofY):
  for j in range(0, numDofX):
    xval = 0.2 + dx*(j-1) + dx/2
    yval = 0.4 + dy*(i-1) + dy/2
    if j==0:
      xval = 0.2
    elif j==numDofX-1:
      xval = 1.0

    if i==0:
      yval = 0.4
    elif i==numDofY-1:
      yval = 1.6

    vertices.append([xval, yval])

print("numDofX: " + str(numDofX))
print("numEleY: " + str(numEleY))
print("numEleX: " + str(numEleX))
      
for i in range(0, numEleY):
  for j in range(0, numEleX):
    #Add cubes to grid
    offsetOne = i*numDofX
    offsetTwo = offsetOne + numDofX 
    vol = [offsetOne + j, offsetOne+1 + j, offsetTwo + j, offsetTwo+1 + j]
    volumesCube.append(vol)
	
    
# write DGF file
outputfile.write("DGF\n")
outputfile.write("Vertex\n")
for vertex in vertices:
	for coord in vertex:
		outputfile.write(str(np.float32(coord)) + " ")
	outputfile.write("\n")	
outputfile.write("#\n")
outputfile.write("CUBE\n")
#outputfile.write("map 0 1 3 2\n")
#outputfile.write("map 0 1 3 2 4 5 7 6\n")
for vol in volumesCube:
    for gvertex in vol:
        outputfile.write(str(gvertex) + ' ')
    outputfile.write("\n")
outputfile.write("#\n")
#outputfile.write("BOUNDARYDOMAIN\ndefault 1\n#")
