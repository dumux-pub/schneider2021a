#!/usr/bin/env python3

import os
import sys
import glob
import subprocess
from math import pow
from argparse import ArgumentParser

try:
    commonPath = os.path.abspath(__file__)
    commonPath = commonPath.rsplit("build-cmake")[0]
    commonPath = os.path.join(commonPath, "test")
    commonPath = os.path.join(commonPath, "common")
    sys.path.append(commonPath)
except Exception:
    sys.exit("Could not find common path")

def exitWithError(msg):
    sys.stderr.write(msg)
    sys.exit(1)


def fileHasExtension(file, ext):
    fileExt = os.path.splitext(file)[1]
    return fileExt == ext or fileExt.strip(".") == ext


def findFilesWithExtension(ext):
    return list(filter(
        lambda f: os.path.isfile(f) and fileHasExtension(f, ext),
        os.listdir()
    ))


def applyToFilesWithExtension(ext, functor):
    for file in findFilesWithExtension(ext):
        functor(file)


def removeFilesWithExtension(ext):
    applyToFilesWithExtension(ext, os.remove)


def moveFilesWithExtension(ext, targetFolder):
    def makeTargetPath(f):
        return os.path.join(targetFolder, f)
    applyToFilesWithExtension(ext, lambda f: os.replace(f, makeTargetPath(f)))


def removePreviousResults():
    removeFilesWithExtension("vtu")
    removeFilesWithExtension("vtp")
    removeFilesWithExtension("pvd")
    removeFilesWithExtension("csv")
    removeFilesWithExtension("tex")


def moveResultsToSubFolder(folderName):
    moveFilesWithExtension("vtu", folderName)
    moveFilesWithExtension("vtp", folderName)
    moveFilesWithExtension("pvd", folderName)
    moveFilesWithExtension("csv", folderName)
    moveFilesWithExtension("tex", folderName)


def compile(exeName):
    subprocess.run(["make", exeName], check=True)

def extractLineDataFromVtp(outputName):
    file_type = '*vtp'
    files = glob.glob('./' + file_type)
    lastOutputFile = max(files, key=os.path.getctime)

    baseCallExtraction = [
    "pvpython", os.path.join(commonPath, "extractlinedatavtp.py"),
    "-f", lastOutputFile,
    "-p1 0.0 1.6 0.0",
    "-p2 1.0 1.6 0.0",
    "-of", outputName
    ]
    subprocess.run(" ".join(str(x) for x in baseCallExtraction), shell=True, executable='/bin/bash')

def generateGmshFiles(ifRefinementFactors, dx=1/80, dy=2/40):
    if not os.path.exists(os.path.join("grids", "simplices.geo")):
        exitWithError("Necessary geo file not found")

    def makeGrid(ifFactor):
        print("Making simplices mesh with interface refinement factor = {}".format(ifFactor))
        baseCall = [
            "gmsh",
            "-setnumber", "iffactor " + str(ifFactor),
            "-setnumber", "dx " + str(dx),
            "-setnumber", "dy " + str(dy),
            "-format", "msh2",
            "-2", "./grids/simplices.geo",
            "-o", "simplices_" + str(factor) + ".msh"
        ]

        subprocess.run(" ".join(str(x) for x in baseCall), shell=True)

    for factor in ifRefinementFactors:
        makeGrid(factor)


def runTest(exeName, outputName, parameterName, parameterValues, extraArgs=[]):
    for val in parameterValues:
        baseCall = [
            './' + exeName, "params.input",
            parameterName, str(val),
            "-Vtk.OutputName", str(outputName) + "_" + str(val)
        ]
        subprocess.run(baseCall + extraArgs, check=True)
        extractLineDataFromVtp(str(outputName) + "_" + str(val))


def runTestAndMoveToFolder(
    exeName, outputName, parameterName, parameterValues, extraArgs=[]
):
    folderName = outputName
    os.makedirs(folderName, exist_ok=True)
    runTest(exeName, outputName, parameterName, parameterValues, extraArgs)
    moveResultsToSubFolder(folderName)


if __name__ == "__main__":
    parser = ArgumentParser(
        description="Result reproduction script for the convergence test."
    )
    #parser.add_argument("-n", "--num-refinements", required=False, default=5)
    args = vars(parser.parse_args())

    #numRefinements = int(args["num_refinements"])
    exeBox = "test_darcy1pbox_stokes1p_vertical"
    exeBoxWeightedDof = "test_darcy1pbox_stokes1p_vertical_weighteddof"
    exeTpfa = "test_darcy1ptpfa_stokes1p_vertical"
    #cubicMeshPrefix = "grids/conforming"

    compile(exeBox)
    compile(exeBoxWeightedDof)
    compile(exeTpfa)
    generateGmshFiles([0.985])
    meshFile = "simplices_0.985.msh"

    removePreviousResults()
    ###########################
    # Box runs
    ###########################

    # Vary Permeability
    runTestAndMoveToFolder(
        exeBox, "box_conforming_permeability", "-SpatialParams.Permeability", [1e-14,1e-12,1e-10,1e-8,1e-6]
    )
    runTestAndMoveToFolder(
        exeBox, "box_boxconforming_permeability", "-SpatialParams.Permeability", [1e-14,1e-12,1e-10,1e-8,1e-6],
        extraArgs=["-Darcy.Grid.File", "./grids/box_conforming.dgf"]
    )
    runTestAndMoveToFolder(
        exeBox, "box_simplices_permeability", "-SpatialParams.Permeability", [1e-14,1e-12,1e-10,1e-8,1e-6],
        extraArgs=["-Darcy.Grid.File", meshFile]
    )
    runTestAndMoveToFolder(
        exeBoxWeightedDof, "box_simplices_permeability_weighteddof", "-SpatialParams.Permeability", [1e-14,1e-12,1e-10,1e-8,1e-6],
        extraArgs=["-Darcy.Grid.File", meshFile]
    )

    # Vary Rynolds number
    runTestAndMoveToFolder(
        exeBox, "box_conforming_Re", "-Stokes.Problem.ReynoldsNumber", [2e2,5e2,1e3,2e3,4e3]
    )
    runTestAndMoveToFolder(
        exeBox, "box_boxconforming_Re", "-Stokes.Problem.ReynoldsNumber", [2e2,5e2,1e3,2e3,4e3],
        extraArgs=["-Darcy.Grid.File", "./grids/box_conforming.dgf"]
    )
    runTestAndMoveToFolder(
        exeBox, "box_simplices_Re", "-Stokes.Problem.ReynoldsNumber", [2e2,5e2,1e3,2e3,4e3],
        extraArgs=["-Darcy.Grid.File", meshFile]
    )
    runTestAndMoveToFolder(
        exeBoxWeightedDof, "box_simplices_Re_weighteddof", "-Stokes.Problem.ReynoldsNumber", [2e2,5e2,1e3,2e3,4e3],
        extraArgs=["-Darcy.Grid.File", meshFile]
    )

    ###########################
    # Tpfa runs
    ###########################

    # Vary Permeability
    runTestAndMoveToFolder(
        exeTpfa, "tpfa_conforming_permeability", "-SpatialParams.Permeability", [1e-14,1e-12,1e-10,1e-8,1e-6]
    )
    runTestAndMoveToFolder(
        exeTpfa, "tpfa_conforming_cm1_permeability", "-SpatialParams.Permeability", [1e-14,1e-12,1e-10,1e-8,1e-6],
        extraArgs=["-Problem.UseCellPressureAtInterface", "true"]
    )

    # Vary Rynolds number
    runTestAndMoveToFolder(
        exeTpfa, "tpfa_conforming_Re", "-Stokes.Problem.ReynoldsNumber", [2e2,5e2,1e3,2e3,4e3]
    )
    runTestAndMoveToFolder(
        exeTpfa, "tpfa_conforming_cm1_Re", "-Stokes.Problem.ReynoldsNumber", [2e2,5e2,1e3,2e3,4e3],
        extraArgs=["-Problem.UseCellPressureAtInterface", "true"]
    )


    ###########################
    # Box runs - vary dx
    ###########################

    dxIfFactors = [0.85, 0.9, 0.95, 0.98, 1.0, 1.1, 1.2]
    numCells = [20, 10]
    lenghts = [1.0,2.0]
    numRefinements = 4

    for i in range(numRefinements):
        generateGmshFiles(dxIfFactors, lenghts[0]/numCells[0], lenghts[1]/numCells[1])
        cells =  str(numCells[0]) + "_" + str(numCells[1])
        gridFiles = ["simplices_" + str(mesh) + ".msh" for mesh in dxIfFactors]
        runTestAndMoveToFolder(
            exeBox, "box_simplices_dxif" + cells, "-Darcy.Grid.File", gridFiles,
            extraArgs=["-Grid.Cells0", str(numCells[0]), "-Grid.Cells1", str(numCells[1])]
        )
        #runTestAndMoveToFolder(
        #    exeBoxWeightedDof, "box_simplices_dxif_weighteddof", "-Darcy.Grid.File", gridFiles
        #)
        numCells[0] = 2*numCells[0]
        numCells[1] = 2*numCells[1]
