#!/bin/bash
echo "Producing results of box and tpfa schemes"
python3 run_paper_simulations.py

echo "Plot total variations"
python3 plot_vy.py

echo "Plot over line with y = 1.6 for chosen test cases"

CSV_FILE="./box_conforming_permeability/box_conforming_permeability_1e-10"
python3 plot_overline.py -f "${CSV_FILE}.csv" \
                         -l "$\Pi^{\mathrm{L2}}$ (conforming)" \
                         -o "box_conforming_permeability_1e-10_plotoverline.pdf"

python3 plot_overline.py -f "${CSV_FILE}.csv" \
                         -l "$\Pi^{\mathrm{L2}}$ (conforming)" \
                         -xlim 0.2 1.0 \
                         -o "box_conforming_permeability_1e-10_plotoverline_zoom.pdf"


CSV_FILE="./box_simplices_permeability/box_simplices_permeability_1e-10"
python3 plot_overline.py -f "${CSV_FILE}.csv" \
                         -l "$\Pi^{\mathrm{L2}}$ (simplices)" \
                         -o "box_simplices_permeability_1e-10_plotoverline.pdf"

python3 plot_overline.py -f "${CSV_FILE}.csv" \
                         -l "$\Pi^{\mathrm{L2}}$ (simplices)" \
                         -xlim 0.2 1.0 \
                         -o "box_simplices_permeability_1e-10_plotoverline_zoom.pdf"



