#!/usr/bin/env python3

import os
import sys
import numpy as np
import matplotlib.pyplot as plt

try:
    commonPath = os.path.abspath(__file__)
    commonPath = commonPath.rsplit("build-cmake")[0]
    commonPath = os.path.join(commonPath, "test")
    commonPath = os.path.join(commonPath, "common")
    sys.path.append(commonPath)
    from mplconfig import plt
except Exception:
    sys.exit("Could not import mplconfig")

def initializePlot():
    plt.figure()
    plt.clf()

def calculateSimplicesFactors(ifRefinementFactors, dx=1/80, dy=2/40):
    lx = 0.8
    factors = np.ceil(((lx-2*dx)/dx) * np.reciprocal(ifRefinementFactors) - 1e-6)
    factors = ((lx-2*dx)/dx) * np.reciprocal(factors)
    return factors

def plotResultsFromSubFolder(subFolder, parameterValues, label, csvFile, extraParams = [False, [1.0,1.01], "b"]):
    addNumOvershoots = extraParams[0]
    offset = extraParams[1]
    col = extraParams[2]

    values = np.zeros_like(parameterValues)
    valuesSign = np.zeros_like(parameterValues)
    for i in range(len(parameterValues)):
        val = parameterValues[i]
        fileName = csvFile(subFolder, parameterValues, i)
        data = np.genfromtxt(fileName, delimiter=',',dtype=float)
        vy = data[data[:,0]>0.2,3]
        tv = np.abs(vy[1:len(vy)]-vy[0:len(vy)-1])
        tvSign = np.abs(np.sign(vy[1:len(vy)])-np.sign(vy[0:len(vy)-1]))
        values[i] = np.sum(tv) / np.max(np.abs(vy))
        valuesSign[i] = 0.5*np.sum(tvSign)
        #vymax = np.max(vy)
        #values[i] = np.max( [vymax, 0.0] ) / np.max(np.abs(vy))

    xAxis = parameterValues
    plt.plot(xAxis, values, label=label, marker='o', color=col)
    setLegendLocation(0.9)
    plt.grid(True)

    if addNumOvershoots:
        for i in range(len(values)):
            plt.text(offset[0]*xAxis[i], offset[1]*values[i], str(int(valuesSign[i])), color=col)

def setAxesLabels(xlabel, ylabel):
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)

def setLegendLocation(scale):
    leg = plt.legend(loc='upper left')
    # Get the bounding box of the original legend
    ax = plt.gca()
    bb = leg.get_bbox_to_anchor().transformed(ax.transAxes.inverted())

    # Change to location of the legend.
    bb.y1 = scale*bb.y1
    leg.set_bbox_to_anchor(bb, transform = ax.transAxes)


def useYLog():
    plt.yscale('log')

def useXLog():
    plt.xscale('log')

def useLogLog():
    plt.yscale('log')
    plt.xscale('log')

def savePlot(name):
    plt.savefig(name, bbox_inches="tight")
    plt.close()


if __name__ == "__main__":
    # this expects run_paper_simulations.py to have been run before
    prop_cycle = plt.rcParams['axes.prop_cycle']
    colors = prop_cycle.by_key()['color']

    def csvFile(subFolder, values, index):
        fileName = os.path.join(subFolder, f"{subFolder}_{str(values[index])}.csv")
        return fileName

    defaultParamsBoxConforming = [True, [0.7,1.05], colors[0]]
    defaultParamsBoxBoxConforming = [True, [0.7,0.8], colors[1]]
    defaultParamsBoxSimplices = [True, [1.0,1.05], colors[2]]
    defaultParamsBoxSimplicesWeightedDof = [False, [1.0,1.05], colors[3]]
    defaultParamsTpfaCM1Conforming = [True, [0.5,1.05], colors[4]]
    defaultParamsTpfaCM4Conforming = [False, [0.7,1.05], colors[5]]

    initializePlot()
    plotResultsFromSubFolder("box_conforming_permeability", [1e-14,1e-12,1e-10,1e-8,1e-6], "$\Pi^{\mathrm{L2}}$ (conforming)", csvFile, defaultParamsBoxConforming)
    plotResultsFromSubFolder("box_boxconforming_permeability", [1e-14,1e-12,1e-10,1e-8,1e-6], "$\Pi^{\mathrm{L2}}$ (box-conforming)", csvFile, defaultParamsBoxBoxConforming)
    plotResultsFromSubFolder("box_simplices_permeability", [1e-14,1e-12,1e-10,1e-8,1e-6], "$\Pi^{\mathrm{L2}}$ (simplices)", csvFile, defaultParamsBoxSimplices)

    #plotResultsFromSubFolder("box_simplices_permeability_weighteddof", [1e-14,1e-12,1e-10,1e-8,1e-6], "$\Pi^{\Gamma}$ (simplices)", defaultParamsBoxSimplicesWeightedDof)

    plotResultsFromSubFolder("tpfa_conforming_cm1_permeability", [1e-14,1e-12,1e-10,1e-8,1e-6], "Tpfa CM1 (conforming)", csvFile, defaultParamsTpfaCM1Conforming)
    plotResultsFromSubFolder("tpfa_conforming_permeability", [1e-14,1e-12,1e-10,1e-8,1e-6], "Tpfa CM4 (conforming)", csvFile, defaultParamsTpfaCM4Conforming)

    #setAxesLabels("permeability", "$v_{\text{ref}}^{-1}\max(\max(v_y),0)$")
    setAxesLabels("permeability [m$^2$]", "$\mathrm{TV}(v^\mathrm{ff}_y) \cdot v_{\mathrm{ref}}^{-1}$ [-]")
    useLogLog()
    savePlot("permeability_overshoots.pdf")

    ##########################################
    defaultParamsBoxConforming = [True, [1.0,1.05], colors[0]]
    defaultParamsBoxBoxConforming = [True, [1.0,0.8], colors[1]]
    defaultParamsBoxSimplices = [True, [1.0,1.05], colors[2]]
    defaultParamsBoxSimplicesWeightedDof = [False, [1.0,1.05], colors[3]]
    defaultParamsTpfaCM1Conforming = [True, [1.0,1.05], colors[4]]
    defaultParamsTpfaCM4Conforming = [False, [1.0,1.05], colors[5]]

    initializePlot()
    plotResultsFromSubFolder("box_conforming_Re", [2e2,5e2,1e3,2e3,4e3], "$\Pi^{\mathrm{L2}}$ (conforming)", csvFile, defaultParamsBoxConforming)
    plotResultsFromSubFolder("box_boxconforming_Re", [2e2,5e2,1e3,2e3,4e3], "$\Pi^{\mathrm{L2}}$ (box-conforming)", csvFile, defaultParamsBoxBoxConforming)
    plotResultsFromSubFolder("box_simplices_Re", [2e2,5e2,1e3,2e3,4e3], "$\Pi^{\mathrm{L2}}$ (simplices)", csvFile, defaultParamsBoxSimplices)

    #plotResultsFromSubFolder("box_simplices_Re_weighteddof", [2e2,5e2,1e3,2e3,4e3], "$\Pi^{\Gamma}$ (simplices)", defaultParamsBoxSimplicesWeightedDof)

    plotResultsFromSubFolder("tpfa_conforming_cm1_Re", [2e2,5e2,1e3,2e3,4e3], "Tpfa CM1 (conforming)", csvFile, defaultParamsTpfaCM1Conforming)
    plotResultsFromSubFolder("tpfa_conforming_Re", [2e2,5e2,1e3,2e3,4e3], "Tpfa CM4 (conforming)", csvFile, defaultParamsTpfaCM4Conforming)

    setAxesLabels("Re [-]", "$\mathrm{TV}(v^\mathrm{ff}_y) \cdot v_{\mathrm{ref}}^{-1}$ [-]")
    useLogLog()
    savePlot("Re_overshoots.pdf")

    ##########################################
    initializePlot()
    dxIfFactors = [0.85, 0.9, 0.95, 0.98, 1.0, 1.1, 1.2]

    def csvFile(subFolder, values, index):
        prefix = str("simplices_")
        suffix = str(".msh")
        fileName = os.path.join(subFolder, f"{subFolder}_{prefix+str(dxIfFactors[index])+suffix}.csv")
        return fileName

    defaultParamsBoxSimplices = [True, [1.0,0.86], colors[0]]
    realIfFactors = calculateSimplicesFactors(dxIfFactors, 1/20, 2/10)
    plotResultsFromSubFolder("box_simplices_dxif20_10", realIfFactors, "refinement 0", csvFile, defaultParamsBoxSimplices)
    defaultParamsBoxSimplices = [False, [0.99,1.05], colors[1]]
    realIfFactors = calculateSimplicesFactors(dxIfFactors, 1/40, 2/20)
    plotResultsFromSubFolder("box_simplices_dxif40_20", realIfFactors, "refinement 1", csvFile, defaultParamsBoxSimplices)
    defaultParamsBoxSimplices = [False, [0.99,1.05], colors[2]]
    realIfFactors = calculateSimplicesFactors(dxIfFactors, 1/80, 2/40)
    plotResultsFromSubFolder("box_simplices_dxif80_40", realIfFactors, "refinement 2", csvFile, defaultParamsBoxSimplices)
    defaultParamsBoxSimplices = [True, [0.99,1.05], colors[3]]
    realIfFactors = calculateSimplicesFactors(dxIfFactors, 1/160, 2/80)
    plotResultsFromSubFolder("box_simplices_dxif160_80", realIfFactors, "refinement 3", csvFile, defaultParamsBoxSimplices)

    setAxesLabels("interface factor $\mathrm{f}_\gamma$ [-]", "$\mathrm{TV}(v^\mathrm{ff}_y) \cdot v_{\mathrm{ref}}^{-1}$ [-]")
    useYLog()
    savePlot("dxif_overshoots.pdf")

