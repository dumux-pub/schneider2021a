// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief A test problem for the coupled Stokes/Darcy channel problem (1p/1p)
 */

#include <config.h>
#include <ctime>
#include <iostream>
#include <fstream>
#include <vector>
#include <numeric>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/timer.hh>
#include <dune/istl/io.hh>
#include <dune/grid/yaspgrid.hh>

#include <dumux/common/properties.hh>
#include <dumux/common/parameters.hh>
#include <dumux/common/partial.hh>
#include <dumux/common/dumuxmessage.hh>
#include <dumux/linear/seqsolverbackend.hh>
#include <dumux/assembly/fvassembler.hh>
#include <dumux/assembly/diffmethod.hh>
#include <dumux/discretization/method.hh>
#include <dumux/discretization/elementsolution.hh>
#include <dumux/discretization/evalgradients.hh>
#include <dumux/io/vtkoutputmodule.hh>
#include <dumux/io/staggeredvtkoutputmodule.hh>
#include <dumux/io/grid/gridmanager.hh>
#include <dumux/io/grid/gridmanager_sub.hh>

#include <dumux/multidomain/staggeredtraits.hh>
#include <dumux/multidomain/fvassembler.hh>
#include <dumux/multidomain/newtonsolver.hh>

#include <dumux/multidomain/boundary/stokesdarcy/couplingmanager.hh>

#include "properties.hh"

namespace Dumux {

//! Computes the cell velocities
template<class Storage, class GV, class Sol>
void computeVelocities(Storage& velocities,
                       const typename GV::GridGeometry& gridGeometry,
                       const GV& gridVariables,
                       const Sol& sol)
{
    auto upwindTerm = [](const auto& volVars) { return volVars.mobility(0); };
    auto fvGeometry = localView(gridGeometry);
    auto elemVolVars = localView(gridVariables.curGridVolVars());

    for (const auto& element : elements(gridGeometry.gridView()))
    {
        const auto eIdx = gridGeometry.elementMapper().index(element);

        // bind local views
        fvGeometry.bind(element);
        elemVolVars.bind(element, fvGeometry, sol);

        // calculate velocity
        const auto elemSol = elementSolution(element, sol, gridGeometry);
        const auto gradP = evalGradients(element,
                                         element.geometry(),
                                         gridGeometry,
                                         elemSol,
                                         element.geometry().center())[0];

        // Do some permeability and mobility weighting
        for (auto&& scv : scvs(fvGeometry))
        {
            const auto& insideVolVars = elemVolVars[scv];
            const auto k = insideVolVars.permeability();
            velocities[eIdx] = -scv.volume() / element.geometry().volume() * upwindTerm(insideVolVars)*mv(k, gradP);
        }
    }
}

} // end namespace Dumux

int main(int argc, char** argv)
{
    using namespace Dumux;

    // initialize MPI, finalize is done automatically on exit
    const auto& mpiHelper = Dune::MPIHelper::instance(argc, argv);

    // print dumux start message
    if (mpiHelper.rank() == 0)
        DumuxMessage::print(/*firstCall=*/true);

    // parse command line arguments and input file
    Parameters::init(argc, argv);

    // Define the sub problem type tags
    using StokesTypeTag = Properties::TTag::NavierStokesDomain;
    using DarcyTypeTag = Properties::TTag::DarcyDomain;

    // The hostgrid
    constexpr int dim = 3;
    using GlobalPosition = Dune::FieldVector<double, dim>;
    using HostGrid = Dune::YaspGrid<dim, Dune::TensorProductCoordinates<double, dim> >;
    using SubGrid = Dune::SubGrid<dim, HostGrid>;
    using HostGridManager = GridManager<HostGrid>;

    HostGridManager stokesHostGridManager;
    stokesHostGridManager.init("FreeFlow");
    auto& stokesHostGrid = stokesHostGridManager.grid();

    struct Params
    {
        GlobalPosition porousMediaBoxMin = getParam<GlobalPosition>("Problem.PorousMediaBoxMin");
        GlobalPosition porousMediaBoxMax = getParam<GlobalPosition>("Problem.PorousMediaBoxMax");
        typename GlobalPosition::value_type lHeight = getParam<typename GlobalPosition::value_type>("Problem.LHeight");
    };

    Params params;

    // The stokes domain subgrid
    auto stokesSelector = [&params](const auto& element)
    {
        double eps = 1e-12;
        GlobalPosition globalPos = element.geometry().center();

        if ( (globalPos[0] < params.porousMediaBoxMax[0] + eps) &&
             ((globalPos[dim-1] > params.porousMediaBoxMax[dim-1] + params.lHeight- eps) ||
              ((globalPos[dim-1] > params.porousMediaBoxMax[dim-1] - eps) && (globalPos[0] > params.porousMediaBoxMin[0] - eps)) ) )
            return true;
        else if ( (globalPos[0] > params.porousMediaBoxMin[0] - eps) &&
                  ((globalPos[dim-1] < params.porousMediaBoxMin[dim-1] - params.lHeight + eps)  ||
                    ((globalPos[dim-1] < params.porousMediaBoxMin[dim-1] + eps) &&  (globalPos[0] < params.porousMediaBoxMax[0] + eps)) ) )
            return true;
        else
            return false;
    };

#if USEUNSTRUCTUREDGRID
    // try to create a grid (from the given grid file or the input file)
    GridManager<GetPropType<DarcyTypeTag, Properties::Grid>> darcyGridManager;
    darcyGridManager.init("PorousMedium");
#else
    HostGridManager darcyHostGridManager;
    darcyHostGridManager.init("PorousMedium");
    auto& darcyHostGrid = darcyHostGridManager.grid();
    // The darcy domain subgrid
    auto darcySelector = [&params](const auto& element)
    {
        double eps = 1e-12;
        GlobalPosition globalPos = element.geometry().center();

        if ( (globalPos[0] > params.porousMediaBoxMin[0] - eps) &&
             (globalPos[0] < params.porousMediaBoxMax[0] + eps) &&
             (globalPos[1] > params.porousMediaBoxMin[1] - eps) &&
             (globalPos[1] < params.porousMediaBoxMax[1] + eps) &&
             (globalPos[2] > params.porousMediaBoxMin[2] - eps) &&
             (globalPos[2] < params.porousMediaBoxMax[2] + eps) )
            return true;
        else
            return false;
    };

    Dumux::GridManager<SubGrid> darcyGridManager;
    darcyGridManager.init(darcyHostGrid, darcySelector);
#endif

    Dumux::GridManager<SubGrid> stokesSubGridManager;
    stokesSubGridManager.init(stokesHostGrid, stokesSelector);

    // we compute on the leaf grid view
    const auto& stokesGridView = stokesSubGridManager.grid().leafGridView();
    const auto& darcyGridView = darcyGridManager.grid().leafGridView();

    // create the finite volume grid geometry
    using StokesFVGridGeometry = GetPropType<StokesTypeTag, Properties::GridGeometry>;
    auto stokesFvGridGeometry = std::make_shared<StokesFVGridGeometry>(stokesGridView);
    using DarcyFVGridGeometry = GetPropType<DarcyTypeTag, Properties::GridGeometry>;
    auto darcyFvGridGeometry = std::make_shared<DarcyFVGridGeometry>(darcyGridView);

    using Traits = Dumux::Properties::FilterTestTraits<
        StaggeredMultiDomainTraits<StokesTypeTag, StokesTypeTag, DarcyTypeTag>
    >;

    // the coupling manager
    using CouplingManager = StokesDarcyCouplingManager<Traits>;
    auto couplingManager = std::make_shared<CouplingManager>(stokesFvGridGeometry, darcyFvGridGeometry);

    // the indices
    constexpr auto freeFlowCellCenterIdx = CouplingManager::freeFlowCellCenterIdx;
    constexpr auto freeFlowFaceIdx = CouplingManager::freeFlowFaceIdx;
    constexpr auto porousMediumIdx = CouplingManager::porousMediumIdx;

    // initialize the fluidsystem (tabulation)
    GetPropType<StokesTypeTag, Properties::FluidSystem>::init();

    // the problem (initial and boundary conditions)
    using StokesProblem = GetPropType<StokesTypeTag, Properties::Problem>;
    auto stokesProblem = std::make_shared<StokesProblem>(stokesFvGridGeometry, couplingManager);
    using DarcyProblem = GetPropType<DarcyTypeTag, Properties::Problem>;
    auto darcyProblem = std::make_shared<DarcyProblem>(darcyFvGridGeometry, couplingManager);

    // get some time loop parameters
    using Scalar = GetPropType<StokesTypeTag, Properties::Scalar>;
    const auto tEnd = getParam<Scalar>("TimeLoop.TEnd");
    const auto maxDt = getParam<Scalar>("TimeLoop.MaxTimeStepSize");
    auto dt = getParam<Scalar>("TimeLoop.DtInitial");

    // instantiate time loop
    auto timeLoop = std::make_shared<TimeLoop<Scalar>>(0, dt, tEnd);
    timeLoop->setMaxTimeStepSize(maxDt);

    stokesProblem->setTimeLoop(timeLoop);

    // the solution vector
    Traits::SolutionVector sol;
    sol[freeFlowCellCenterIdx].resize(stokesFvGridGeometry->numCellCenterDofs());
    sol[freeFlowFaceIdx].resize(stokesFvGridGeometry->numFaceDofs());
    sol[porousMediumIdx].resize(darcyFvGridGeometry->numDofs());

    // apply initial solution for instationary problems
    stokesProblem->applyInitialSolution(sol);
    darcyProblem->applyInitialSolution(sol[porousMediumIdx]);

    auto solOld = sol;
    auto stokesSol = partial(sol, freeFlowFaceIdx, freeFlowCellCenterIdx);

    couplingManager->init(stokesProblem, darcyProblem, sol);

    // the grid variables
    using StokesGridVariables = GetPropType<StokesTypeTag, Properties::GridVariables>;
    auto stokesGridVariables = std::make_shared<StokesGridVariables>(stokesProblem, stokesFvGridGeometry);
    stokesGridVariables->init(stokesSol);
    using DarcyGridVariables = GetPropType<DarcyTypeTag, Properties::GridVariables>;
    auto darcyGridVariables = std::make_shared<DarcyGridVariables>(darcyProblem, darcyFvGridGeometry);
    darcyGridVariables->init(sol[porousMediumIdx]);

    // intialize the vtk output modules
    StaggeredVtkOutputModule<StokesGridVariables, decltype(stokesSol)> stokesVtkWriter(*stokesGridVariables,
                                                                                       stokesSol,
                                                                                       stokesProblem->name());
    GetPropType<StokesTypeTag, Properties::IOFields>::initOutputModule(stokesVtkWriter);
    stokesVtkWriter.write(0.0);

    VtkOutputModule<DarcyGridVariables, GetPropType<DarcyTypeTag, Properties::SolutionVector>> darcyVtkWriter(*darcyGridVariables,
                                                                                                              sol[porousMediumIdx],
                                                                                                              darcyProblem->name());
    using DarcyVelocityOutput = GetPropType<DarcyTypeTag, Properties::VelocityOutput>;
    darcyVtkWriter.addVelocityOutput(std::make_shared<DarcyVelocityOutput>(*darcyGridVariables));
    GetPropType<DarcyTypeTag, Properties::IOFields>::initOutputModule(darcyVtkWriter);

    std::vector<GlobalPosition> velocities(darcyGridView.size(0), GlobalPosition(0.0));
    computeVelocities(velocities, *darcyFvGridGeometry, *darcyGridVariables, sol[porousMediumIdx]);
    darcyVtkWriter.addField(velocities, "cellVelocity");

    darcyVtkWriter.write(0.0);

    // the assembler with time loop for instationary problem
    using Assembler = MultiDomainFVAssembler<Traits, CouplingManager, DiffMethod::numeric>;
    auto assembler = std::make_shared<Assembler>(std::make_tuple(stokesProblem, stokesProblem, darcyProblem),
                                                 std::make_tuple(stokesFvGridGeometry->faceFVGridGeometryPtr(),
                                                                 stokesFvGridGeometry->cellCenterFVGridGeometryPtr(),
                                                                 darcyFvGridGeometry),
                                                 std::make_tuple(stokesGridVariables->faceGridVariablesPtr(),
                                                                 stokesGridVariables->cellCenterGridVariablesPtr(),
                                                                 darcyGridVariables),
                                                 couplingManager,
                                                 timeLoop,
                                                 solOld);

    // the linear solver
    using LinearSolver = UMFPackBackend;
    auto linearSolver = std::make_shared<LinearSolver>();

    // the non-linear solver
    using NewtonSolver = MultiDomainNewtonSolver<Assembler, LinearSolver, CouplingManager>;
    NewtonSolver nonLinearSolver(assembler, linearSolver, couplingManager);

    // time loop
    timeLoop->start(); do
    {
        // solve the non-linear system with time step control
        nonLinearSolver.solve(sol, *timeLoop);

        // make the new solution the old solution
        solOld = sol;
        stokesGridVariables->advanceTimeStep();
        darcyGridVariables->advanceTimeStep();

        // advance to the time loop to the next step
        timeLoop->advanceTimeStep();

        computeVelocities(velocities, *darcyFvGridGeometry, *darcyGridVariables, sol[porousMediumIdx]);

        // write vtk output
        stokesVtkWriter.write(timeLoop->time());
        darcyVtkWriter.write(timeLoop->time());

        // report statistics of this time step
        timeLoop->reportTimeStep();

        // set new dt as suggested by newton solver
        timeLoop->setTimeStepSize(nonLinearSolver.suggestTimeStepSize(timeLoop->timeStepSize()));

    } while (!timeLoop->finished());

    timeLoop->finalize(stokesGridView.comm());
    timeLoop->finalize(darcyGridView.comm());

    ////////////////////////////////////////////////////////////
    // finalize, print dumux message to say goodbye
    ////////////////////////////////////////////////////////////

    // print dumux end message
    if (mpiHelper.rank() == 0)
    {
        Parameters::print();
        DumuxMessage::print(/*firstCall=*/false);
    }

    return 0;
} // end main
