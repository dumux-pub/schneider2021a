// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup BoundaryTests
 * \brief A simple Stokes test problem for the staggered grid (Navier-)Stokes model.
 */

#ifndef DUMUX_FREEFLOW_1P__FILTER_SUBPROBLEM_HH
#define DUMUX_FREEFLOW_1P_FILTER_SUBPROBLEM_HH

#include <dumux/common/properties.hh>
#include <dumux/common/parameters.hh>
#include <dumux/common/numeqvector.hh>
#include <dumux/common/timeloop.hh>

#include <dumux/freeflow/navierstokes/boundarytypes.hh>
#include <dumux/freeflow/navierstokes/problem.hh>
#include <dumux/freeflow/navierstokes/fluxhelper.hh>

namespace Dumux {

/*!
 * \brief The rans subproblem
 */
template <class TypeTag>
class StokesSubProblem : public NavierStokesProblem<TypeTag>
{
    using ParentType = NavierStokesProblem<TypeTag>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using FluidSystem = GetPropType<TypeTag, Properties::FluidSystem>;
    using FluidState = GetPropType<TypeTag, Properties::FluidState>;
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using GridView = typename GridGeometry::GridView;
    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using NumEqVector = Dumux::NumEqVector<PrimaryVariables>;

    using ModelTraits = GetPropType<TypeTag, Properties::ModelTraits>;
    using Indices = typename ModelTraits::Indices;
    using Element = typename GridGeometry::GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;
    using FVElementGeometry = typename GridGeometry::LocalView;
    using BoundaryTypes = Dumux::NavierStokesBoundaryTypes<GetPropType<TypeTag, Properties::ModelTraits>::numEq()>;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;
    using SubControlVolumeFace = typename FVElementGeometry::SubControlVolumeFace;

    using CouplingManager = GetPropType<TypeTag, Properties::CouplingManager>;
    using TimeLoopPtr = std::shared_ptr<TimeLoop<Scalar>>;

    using DiffusionCoefficientAveragingType = typename StokesDarcyCouplingOptions::DiffusionCoefficientAveragingType;
    using FluxHelper = NavierStokes::BoundaryFluxHelper<ModelTraits, NumEqVector>;

    static constexpr auto dimWorld = GridGeometry::GridView::dimensionworld;

public:
    StokesSubProblem(std::shared_ptr<const GridGeometry> gridGeometry,
                     std::shared_ptr<CouplingManager> couplingManager)
    : ParentType(gridGeometry, "FreeFlow")
    , eps_(1e-8)
    , couplingManager_(couplingManager)
    {
#if NONISOTHERMAL
        diffCoeffAvgType_ = StokesDarcyCouplingOptions::stringToEnum(DiffusionCoefficientAveragingType{},
                                                                getParamFromGroup<std::string>(this->paramGroup(), "Problem.InterfaceDiffusionCoefficientAvg"));

#endif
        problemName_  =  getParam<std::string>("Vtk.OutputName") + "_" + getParamFromGroup<std::string>(this->paramGroup(), "Problem.Name");

        deltaP_ = getParam<Scalar>("Problem.DeltaP");

        porousMediaBoxMin_ = getParam<GlobalPosition>("Problem.PorousMediaBoxMin");
        porousMediaBoxMax_ = getParam<GlobalPosition>("Problem.PorousMediaBoxMax");
    }

    const std::string& name() const
    { return problemName_; }

    /*!
     * \brief Returns the temperature [K] within the domain for the isothermal model.
     */
    Scalar temperature() const
    { return 298.15; }

    /*!
     * \brief Returns the sources within the domain.
     *
     * \param globalPos The global position
     */
    NumEqVector sourceAtPos(const GlobalPosition &globalPos) const
    { return NumEqVector(0.0); }

    /*!
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary control volume.
     *
     * \param globalPos The position of the center of the finite volume
     */
    BoundaryTypes boundaryTypes(const Element& element,
                                const SubControlVolumeFace& scvf) const
    {
        BoundaryTypes values;
        const auto& globalPos = scvf.dofPosition();

        if (onRightBoundary_(globalPos))
        {
            values.setDirichlet(Indices::pressureIdx);
#if NONISOTHERMAL
            values.setNeumann(Indices::temperatureIdx);
#endif
        }
        else if (onLeftBoundary_(globalPos))
        {
            values.setDirichlet(Indices::pressureIdx);
#if NONISOTHERMAL
            values.setDirichlet(Indices::temperatureIdx);
#endif
        }
        else
        {
            values.setNeumann(Indices::conti0EqIdx);
            values.setDirichlet(Indices::velocityXIdx);
            values.setDirichlet(Indices::velocityYIdx);
            values.setDirichlet(Indices::velocityZIdx);
#if NONISOTHERMAL
            values.setNeumann(Indices::energyEqIdx);
#endif
        }

        if (isOnCouplingWall_(scvf))
        {
            values.setCouplingNeumann(Indices::conti0EqIdx);
            values.setCouplingNeumann(Indices::velocityZIdx);
            values.setSlipCondition(Indices::velocityXIdx);
            values.setSlipCondition(Indices::velocityYIdx);
#if NONISOTHERMAL
            values.setNeumann(Indices::energyEqIdx);
#endif
        }

        return values;
    }

    /*!
     * \brief Evaluate the boundary conditions for a dirichlet
     *        control volume.
     *
     * \param globalPos The center of the finite volume which ought to be set.
     */
    PrimaryVariables dirichletAtPos(const GlobalPosition& globalPos) const
    {
        PrimaryVariables values(0.0);
        values = initialAtPos(globalPos);

        if(onLeftBoundary_(globalPos))
        {
            values[Indices::pressureIdx] += deltaP_;
#if NONISOTHERMAL
            values[Indices::temperatureIdx] = temperature() + 20;
#endif
        }

        return values;
    }

    /*!
     * \brief Evaluates the boundary conditions for a Neumann control volume.
     *
     * \param element The element for which the Neumann boundary condition is set
     * \param fvGeometry The fvGeometry
     * \param elemVolVars The element volume variables
     * \param elemFaceVars The element face variables
     * \param scvf The boundary sub control volume face
     */
    template<class ElementVolumeVariables, class ElementFaceVariables>
    NumEqVector neumann(const Element& element,
                        const FVElementGeometry& fvGeometry,
                        const ElementVolumeVariables& elemVolVars,
                        const ElementFaceVariables& elemFaceVars,
                        const SubControlVolumeFace& scvf) const
    {
        NumEqVector values(0.0);

        if(couplingManager().isCoupledEntity(CouplingManager::freeFlowIdx, scvf))
        {
            values[Indices::momentumZBalanceIdx] = couplingManager().couplingData().momentumCouplingCondition(element, fvGeometry, elemVolVars, elemFaceVars, scvf);
            values[Indices::conti0EqIdx] = couplingManager().couplingData().massCouplingCondition(element, fvGeometry, elemVolVars, elemFaceVars, scvf);
#if NONISOTHERMAL
            values[Indices::energyEqIdx] = couplingManager().couplingData().energyCouplingCondition(element, fvGeometry, elemVolVars, elemFaceVars, scvf, diffCoeffAvgType_);
#endif
        }
        else if(onRightBoundary_(scvf.dofPosition()))
        {
#if NONISOTHERMAL
            values[Indices::energyEqIdx] = FluxHelper::outflowFlux(*this,
                                                                   element,
                                                                   fvGeometry,
                                                                   elemVolVars[scvf.insideScvIdx()],
                                                                   initialAtPos(scvf.center()),
                                                                   scvf,
                                                                   elemFaceVars[scvf].velocitySelf())[Indices::energyEqIdx];
#endif
        }
        return values;
    }

    /*!
     * \brief Evaluate the initial value for a control volume.
     *
     * \param globalPos The global position
     */
    PrimaryVariables initialAtPos(const GlobalPosition& globalPos) const
    {
        PrimaryVariables values(0.0);
        values[Indices::pressureIdx] = 1e5;

#if NONISOTHERMAL
        values[Indices::temperatureIdx] = temperature();
#endif

        return values;
    }

    //! Returns the intrinsic permeability of required as input parameter for the Beavers-Joseph-Saffman boundary condition
    auto permeability(const Element& element, const SubControlVolumeFace& scvf) const
    { return couplingManager().couplingData().darcyPermeability(element, scvf); }

        //! Returns the alpha value required as input parameter for the Beavers-Joseph-Saffman boundary condition
    Scalar alphaBJ(const SubControlVolumeFace& scvf) const
    { return couplingManager().problem(CouplingManager::porousMediumIdx).spatialParams().beaversJosephCoeffAtPos(scvf.center()); }

    // \}
    /*!
     * \brief Sets the time loop pointer.
     */
    void setTimeLoop(TimeLoopPtr timeLoop)
    { timeLoop_ = timeLoop; }

    /*!
     * \brief Returns the time.
     */
    Scalar time() const
    { return timeLoop_->time(); }

    //! Set the coupling manager
    void setCouplingManager(std::shared_ptr<CouplingManager> cm)
    { couplingManager_ = cm; }

    //! Get the coupling manager
    const CouplingManager& couplingManager() const
    { return *couplingManager_; }

private:
    bool onLeftBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[0] < this->gridGeometry().bBoxMin()[0] + eps_; }

    bool onRightBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[0] > this->gridGeometry().bBoxMax()[0] - eps_; }

    bool isOnCouplingWall_(const SubControlVolumeFace& scvf) const
    { return couplingManager().isCoupledEntity(CouplingManager::freeFlowIdx, scvf); }

    Scalar deltaP_;
    Scalar inletMoleFraction_;
    GlobalPosition porousMediaBoxMin_;
    GlobalPosition porousMediaBoxMax_;

    Scalar eps_;
    std::string problemName_;
    std::shared_ptr<CouplingManager> couplingManager_;
    TimeLoopPtr timeLoop_;
#if NONISOTHERMAL
    DiffusionCoefficientAveragingType diffCoeffAvgType_;
#endif
};

} // end namespace Dumux

#endif
