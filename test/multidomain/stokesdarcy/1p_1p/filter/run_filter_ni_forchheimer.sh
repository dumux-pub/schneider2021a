#!/bin/bash

# make grid
gmsh -format msh2 \
     -setnumber curvature 0.5 \
     -setnumber dx 0.01125 \
     -setnumber dx_interface 0.0075 \
     -3 ../../../../../../test/common/filter.geo -o filter.msh

# compile & execute
EXE="test_coupled_1pni_1pni_filter_forchheimer_unstructured"
make $EXE
./$EXE params_unstructured_ni.input -PorousMedium.Grid.File filter.msh -FreeFlow.Grid.Refinement 1
