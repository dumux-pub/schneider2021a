// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup BoundaryTests
 * \brief The spatial parameters class for the test problem using the 1p cc model.
 */

#ifndef DUMUX_FILTER_SPATIALPARAMS_HH
#define DUMUX_FILTER_SPATIALPARAMS_HH

#include <dumux/material/spatialparams/fv1p.hh>

namespace Dumux
{

/*!
 * \ingroup BoundaryTests
 * \brief The spatial parameters class for the test problem using the
 *        1p cc model.
 */
template<class GridGeometry, class Scalar>
class DarcySpatialParams
: public FVSpatialParamsOneP<GridGeometry, Scalar,
                             DarcySpatialParams<GridGeometry, Scalar>>
{
    using GridView = typename GridGeometry::GridView;
    using ParentType = FVSpatialParamsOneP<GridGeometry, Scalar,
                                           DarcySpatialParams<GridGeometry, Scalar>>;

    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

    static constexpr int dimWorld = GridView::dimensionworld;
    using DimWorldMatrix = Dune::FieldMatrix<Scalar, dimWorld, dimWorld>;

public:
    // export permeability type
    using PermeabilityType = DimWorldMatrix;

    DarcySpatialParams(std::shared_ptr<const GridGeometry> gridGeometry)
        : ParentType(gridGeometry)
    {
        // rotate permeability such that first main direction points to (1, 1, 1)
        permeability_ = 0.0;
        permeability_[0][0] = getParam<Scalar>("PorousMedium.SpatialParams.PermeabilityE0");
        permeability_[1][1] = getParam<Scalar>("PorousMedium.SpatialParams.PermeabilityE1");
        permeability_[2][2] = getParam<Scalar>("PorousMedium.SpatialParams.PermeabilityE2");

        const auto angleZ = getParam<Scalar>("PorousMedium.SpatialParams.PermAngleAroundZ");
        const auto angleXY = getParam<Scalar>("PorousMedium.SpatialParams.PermAngleAroundXY");

        auto rotMatrix = getRotationMatrix_({{0.0, 0.0, 1.0}}, angleZ);
        const auto firstRotatedYAxis = mv(rotMatrix, GlobalPosition{{0.0, 1.0, 0.0}});

        rotMatrix.rightmultiply(getRotationMatrix_(firstRotatedYAxis, angleXY));
        auto rotMatrixInverse = rotMatrix;
        rotMatrixInverse.invert();

        permeability_.rightmultiply(rotMatrix);
        permeability_.leftmultiply(rotMatrixInverse);

        std::cout << "Main directions of rotated permeability tensor:" << std::endl;
        std::cout << mv(rotMatrix, GlobalPosition{{1.0, 0.0, 0.0}}) << std::endl;
        std::cout << mv(rotMatrix, GlobalPosition{{0.0, 1.0, 0.0}}) << std::endl;
        std::cout << mv(rotMatrix, GlobalPosition{{0.0, 0.0, 1.0}}) << std::endl;

        alphaBJ_ = getParam<Scalar>("PorousMedium.SpatialParams.AlphaBeaversJoseph");
        porosity_ = getParam<Scalar>("PorousMedium.SpatialParams.Porosity");
    }

    /*!
     * \brief Function for defining the (intrinsic) permeability \f$[m^2]\f$.
     *
     * \param globalPos The global position
     * \return the intrinsic permeability
     */
    PermeabilityType permeabilityAtPos(const GlobalPosition& globalPos) const
    { return permeability_; }

    /*! \brief Defines the porosity in [-].
     *
     * \param globalPos The global position
     */
    Scalar porosityAtPos(const GlobalPosition& globalPos) const
    { return porosity_; }

    /*! \brief Defines the Beavers-Joseph coefficient in [-].
     *
     * \param globalPos The global position
     */
    Scalar beaversJosephCoeffAtPos(const GlobalPosition& globalPos) const
    { return alphaBJ_; }

private:
    // see https://de.wikipedia.org/wiki/Drehmatrix
    DimWorldMatrix getRotationMatrix_(GlobalPosition rotationAxis, Scalar angle) const
    {
        // make sure axis is of unit length
        rotationAxis /= rotationAxis.two_norm();
        const auto rotX = rotationAxis[0];
        const auto rotY = rotationAxis[1];
        const auto rotZ = rotationAxis[2];

        using std::sin;
        using std::cos;
        const auto sinAngle = sin(angle);
        const auto cosAngle = cos(angle);
        const auto oneMinusCosAngle = 1.0 - cosAngle;

        return {{
            {rotX*rotX*oneMinusCosAngle + cosAngle,
             rotX*rotY*oneMinusCosAngle - rotZ*sinAngle,
             rotX*rotZ*oneMinusCosAngle + rotY*sinAngle},
            {rotX*rotY*oneMinusCosAngle + rotZ*sinAngle,
             rotY*rotY*oneMinusCosAngle + cosAngle,
             rotY*rotZ*oneMinusCosAngle - rotX*sinAngle},
            {rotX*rotZ*oneMinusCosAngle - rotY*sinAngle,
             rotY*rotZ*oneMinusCosAngle + rotX*sinAngle,
             rotZ*rotZ*oneMinusCosAngle + cosAngle},
        }};
    }

    PermeabilityType permeability_;
    Scalar alphaBJ_;
    Scalar porosity_;
};

} // end namespace Dumux

#endif
