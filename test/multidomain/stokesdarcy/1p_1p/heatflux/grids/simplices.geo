xmin = 0.2;
ymin = 1.4;
xmax = 1.0;
ymax = 2.6;

lx = xmax-xmin;
ly = ymax-ymin;

dx = 1.0/80;
dy = 3.0/60;

Point(1) = {xmin, ymin, 0.0};
Point(2) = {xmin, ymax, 0.0};
Point(3) = {xmax, ymax, 0.0};
Point(4) = {xmax, ymin, 0.0};

Point(5) = {xmin + dx/2, ymin, 0.0};
Point(6) = {xmin, ymin + dy/2, 0.0};
Point(7) = {xmin, ymax-dy/2, 0.0};
Point(8) = {xmin + dx/2, ymax, 0.0};
Point(9) = {xmax - dx/2, ymax, 0.0};
Point(10) = {xmax, ymax-dy/2, 0.0};
Point(11) = {xmax, ymin + dy/2, 0.0};
Point(12) = {xmax - dx/2, ymin, 0.0};

Line(1) = {1, 6};
Line(2) = {6, 7};
Line(3) = {7, 2};
Line(4) = {2, 8};
Line(5) = {8, 9};
Line(6) = {9, 3};
Line(7) = {3, 10};
Line(8) = {10, 11};
Line(9) = {11, 4};
Line(10) = {4, 12};
Line(11) = {12, 5};
Line(12) = {5, 1};

Transfinite Line {1,3,4,6,7,8,10,12} = 1 Using Progression 1;
Transfinite Line {5,11} = Round(lx/dx) Using Progression 1;
Transfinite Line {2,8} = Round(ly/dy) Using Progression 1;

Line Loop(1) = {1:12};
Plane Surface(1) = {1};



