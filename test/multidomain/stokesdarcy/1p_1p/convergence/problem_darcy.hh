// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup BoundaryTests
 * \brief A simple Darcy test problem (cell-centered finite volume method).
 */

#ifndef DUMUX_DARCY_CONV_SUBPROBLEM_HH
#define DUMUX_DARCY_CONV_SUBPROBLEM_HH

#include <fstream>
#include <dune/grid/yaspgrid.hh>

#include <dumux/discretization/cctpfa.hh>
#include <dumux/discretization/box.hh>
#include <dumux/common/numeqvector.hh>

#include <dumux/porousmediumflow/1p/model.hh>
#include <dumux/porousmediumflow/problem.hh>

#include "spatialparams.hh"

#include <dumux/material/components/constant.hh>
#include <dumux/material/fluidsystems/1pliquid.hh>

#include <dune/geometry/quadraturerules.hh>

#ifndef DARCYGRIDTYPE
#define DARCYGRIDTYPE Dune::YaspGrid<2>
#endif

namespace Dumux {
template <class TypeTag>
class DarcySubProblem;

namespace Properties {
// Create new type tags
namespace TTag {
struct DarcyOneP { using InheritsFrom = std::tuple<OneP>; };
struct DarcyOnePTpfa { using InheritsFrom = std::tuple<DarcyOneP, CCTpfaModel>; };
struct DarcyOnePBox { using InheritsFrom = std::tuple<DarcyOneP, BoxModel>; };
} // end namespace TTag

// Set the problem property
template<class TypeTag>
struct Problem<TypeTag, TTag::DarcyOneP> { using type = Dumux::DarcySubProblem<TypeTag>; };

// the fluid system
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::DarcyOneP>
{
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using type = FluidSystems::OnePLiquid<Scalar, Components::Constant<1, Scalar> >;
};

// Set the grid type
template<class TypeTag>
struct Grid<TypeTag, TTag::DarcyOneP> { using type = DARCYGRIDTYPE; };

template<class TypeTag>
struct SpatialParams<TypeTag, TTag::DarcyOneP>
{
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using type = OnePSpatialParams<GridGeometry, Scalar>;
};
} // end namespace Properties

template <class TypeTag>
class DarcySubProblem : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;
    using GridView = typename GetPropType<TypeTag, Properties::GridGeometry>::GridView;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using BoundaryTypes = Dumux::BoundaryTypes<GetPropType<TypeTag, Properties::ModelTraits>::numEq()>;
    using VolumeVariables = GetPropType<TypeTag, Properties::VolumeVariables>;
    using FVElementGeometry = typename GetPropType<TypeTag, Properties::GridGeometry>::LocalView;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;
    using SubControlVolumeFace = typename FVElementGeometry::SubControlVolumeFace;
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using NumEqVector = Dumux::NumEqVector<PrimaryVariables>;

    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

    using CouplingManager = GetPropType<TypeTag, Properties::CouplingManager>;

    static constexpr auto velocityXIdx = 0;
    static constexpr auto velocityYIdx = 1;
    static constexpr auto pressureIdx = 2;

public:
    //! export the Indices
    using Indices = typename GetPropType<TypeTag, Properties::ModelTraits>::Indices;

    DarcySubProblem(std::shared_ptr<const GridGeometry> gridGeometry,
                   std::shared_ptr<CouplingManager> couplingManager)
    : ParentType(gridGeometry, "Darcy"), eps_(1e-7), couplingManager_(couplingManager)
    {
        problemName_  =  getParam<std::string>("Vtk.OutputName") + "_" + getParamFromGroup<std::string>(this->paramGroup(), "Problem.Name");

        omega_ = getParam<Scalar>("Problem.FreqFactor")*M_PI;
        c_ = getParam<Scalar>("Problem.PermFactor");
    }

    /*!
     * \brief The problem name.
     */
    const std::string& name() const
    {
        return problemName_;
    }

    /*!
     * \name Problem parameters
     */
    // \{

    /*!
     * \brief Returns the temperature within the domain in [K].
     *
     */
    Scalar temperature() const
    { return 273.15 + 10; } // 10°C
    // \}

    /*!
     * \name Boundary conditions
     */
    // \{

    /*!
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary segment.
     *
     * \param element The finite element
     * \param scvf The sub control volume face
     */
    BoundaryTypes boundaryTypes(const Element &element,
                                const SubControlVolumeFace &scvf) const
    {
        BoundaryTypes values;

        values.setAllNeumann();

        if(onLeftBoundary_(scvf.center()) || onRightBoundary_(scvf.center()))
        {
            values.setAllDirichlet();
            return values;
        }

        if (couplingManager().isCoupledEntity(CouplingManager::porousMediumIdx, element, scvf))
            values.setAllCouplingNeumann();

        return values;
    }


    /*!
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary segment.
     *
     * \param element The finite element
     * \param scv The sub control volume
     */
    BoundaryTypes boundaryTypes(const Element &element,
                                const SubControlVolume &scv) const
    {
        BoundaryTypes values;

        values.setAllNeumann();

        if(onLeftBoundary_(scv.dofPosition()) || onRightBoundary_(scv.dofPosition()))
        {
            values.setAllDirichlet();
            return values;
        }

        auto fvGeometry = localView(this->gridGeometry());
        fvGeometry.bindElement(element);
        for (auto&& scvf : scvfs(fvGeometry))
        {
            if (couplingManager().isCoupledEntity(CouplingManager::porousMediumIdx, element, scvf))
                values.setAllCouplingNeumann();
        }

        return values;
    }

    /*!
     * \brief Evaluates the boundary conditions for a Dirichlet boundary segment.
     *
     * \param globalPos The position for which the Dirichlet value is set
     *
     * For this method, the \a values parameter stores primary variables.
     */
    PrimaryVariables dirichletAtPos(const GlobalPosition &globalPos) const
    {
        return exactPressure(globalPos);
    }

    /*!
     * \brief Evaluates the boundary conditions for a Neumann control volume.
     *
     * \param element The element for which the Neumann boundary condition is set
     * \param fvGeometry The fvGeometry
     * \param elemVolVars The element volume variables
     * \param elemFluxVarsCache Flux variables caches for all faces in stencil
     * \param scvf The boundary sub control volume face
     *
     * For this method, the \a values variable stores primary variables.
     */
    template<class ElementVolumeVariables, class ElementFluxVarsCache>
    NumEqVector neumann(const Element& element,
                        const FVElementGeometry& fvGeometry,
                        const ElementVolumeVariables& elemVolVars,
                        const ElementFluxVarsCache& elemFluxVarsCache,
                        const SubControlVolumeFace& scvf) const
    {
        NumEqVector values(0.0);

        // we assume that the density and viscosity is 1.0
        values = exactVelocity(scvf.ipGlobal())*scvf.unitOuterNormal();

        if (couplingManager().isCoupledEntity(CouplingManager::porousMediumIdx, element, scvf))
        {
            values[Indices::conti0EqIdx] = couplingManager().couplingData().massCouplingCondition(element, fvGeometry, elemVolVars, elemFluxVarsCache, scvf);
        }

        return values;
    }

    // \}

    /*!
     * \name Volume terms
     */
    // \{
    /*!
     * \brief Evaluates the source term for all phases within a given
     *        sub control volume.
     *
     * \param element The element for which the source term is set
     * \param fvGeometry The fvGeometry
     * \param elemVolVars The element volume variables
     * \param scv The sub control volume
     */
    template<class ElementVolumeVariables>
    NumEqVector source(const Element &element,
                       const FVElementGeometry& fvGeometry,
                       const ElementVolumeVariables& elemVolVars,
                       const SubControlVolume &scv) const
    {
        NumEqVector source(0.0);

        using std::cos;
        using std::sin;
        using std::exp;

        const auto& quad = Dune::QuadratureRules<Scalar, GridView::dimension>::rule(scv.geometry().type(), 5);

        for (auto&& qp : quad)
        {
            GlobalPosition globalPos = scv.geometry().global(qp.position());
            Scalar x = globalPos[0];
            Scalar y = globalPos[1];

            auto integrationElement = scv.geometry().integrationElement(qp.position());

            source[Indices::conti0EqIdx] += (-(c_*cos(omega_*x) + 1)*exp(y - 1)
                                             + 1.5*c_*exp(y + 1)*cos(omega_*x)
                                             + omega_*omega_*(exp(y + 1) - exp(2) + 2))
                                             *sin(omega_*x)*qp.weight()*integrationElement;
        }
        source[Indices::conti0EqIdx] /= scv.volume();

        return source;
    }

    // \}

    /*!
     * \brief Evaluate the initial value for a control volume.
     *
     * \param globalPos The global position
     */
    PrimaryVariables initialAtPos(const GlobalPosition &globalPos) const
    {
        return exactPressure(globalPos);
    }

    // \}

    //! Get the coupling manager
    const CouplingManager& couplingManager() const
    { return *couplingManager_; }

    /*!
    * \brief Return the analytical solution of the problem at a given position
    *
    * \param globalPos The global position
    */
    Dune::FieldVector<Scalar, 3> analyticalSolution(const GlobalPosition& globalPos) const
    {
        Dune::FieldVector<Scalar, 3> values;
        values[pressureIdx] = exactPressure(globalPos);

        auto velocity = exactVelocity(globalPos);
        values[velocityXIdx] = velocity[0];
        values[velocityYIdx] = velocity[1];

        return values;
    }

private:

    bool onLowerBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[1] < this->gridGeometry().bBoxMin()[1] + eps_; }

    bool onLeftBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[0] < this->gridGeometry().bBoxMin()[0] + eps_; }

    bool onRightBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[0] > this->gridGeometry().bBoxMax()[0] - eps_; }

    Scalar exactPressure(const GlobalPosition &globalPos) const
    {
        using std::exp;
        using std::sin;
        Scalar x = globalPos[0];
        Scalar y = globalPos[1];
        return (exp(y+1) + 2 - exp(2))*sin(omega_*x);
    }

    GlobalPosition exactVelocity(const GlobalPosition &globalPos) const
    {
        using std::sin;
        using std::cos;
        using std::exp;
        Scalar x = globalPos[0];
        Scalar y = globalPos[1];
        GlobalPosition velocity(0.0);
        velocity[0] = c_/(2*omega_)*exp(y+1)*sin(omega_*x)*sin(omega_*x)
                     -omega_*(exp(y+1) + 2 - exp(2))*cos(omega_*x);
        velocity[1] = (0.5*c_*(exp(y+1) + 2 - exp(2))*cos(omega_*x)
                       -(c_*cos(omega_*x) + 1)*exp(y-1))*sin(omega_*x);

        return velocity;
    }

    Scalar eps_;
    std::shared_ptr<CouplingManager> couplingManager_;
    std::string problemName_;
    Scalar omega_;
    Scalar c_;
};
} // end namespace Dumux

#endif //DUMUX_DARCY_SUBPROBLEM_HH
