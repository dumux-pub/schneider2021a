#!/usr/bin/env python3

import os
import sys
import subprocess
from math import pow
from argparse import ArgumentParser

def exitWithError(msg):
    sys.stderr.write(msg)
    sys.exit(1)


def fileHasExtension(file, ext):
    fileExt = os.path.splitext(file)[1]
    return fileExt == ext or fileExt.strip(".") == ext


def findFilesWithExtension(ext):
    return list(filter(
        lambda f: os.path.isfile(f) and fileHasExtension(f, ext),
        os.listdir()
    ))


def applyToFilesWithExtension(ext, functor):
    for file in findFilesWithExtension(ext):
        functor(file)


def removeFilesWithExtension(ext):
    applyToFilesWithExtension(ext, os.remove)


def moveFilesWithExtension(ext, targetFolder):
    def makeTargetPath(f):
        return os.path.join(targetFolder, f)
    applyToFilesWithExtension(ext, lambda f: os.replace(f, makeTargetPath(f)))


def removePreviousResults():
    removeFilesWithExtension("vtu")
    removeFilesWithExtension("vtp")
    removeFilesWithExtension("pvd")
    removeFilesWithExtension("log")
    removeFilesWithExtension("tex")


def moveResultsToSubFolder(folderName):
    moveFilesWithExtension("vtu", folderName)
    moveFilesWithExtension("vtp", folderName)
    moveFilesWithExtension("pvd", folderName)
    moveFilesWithExtension("log", folderName)
    moveFilesWithExtension("tex", folderName)


def prepareCubicMeshes(numRefinements, meshFilePrefix):
    if not os.path.exists(os.path.join("grids", "cubicMesh.py")):
        exitWithError("This has to be called from the convergence test folder")

    def makeGrid(dx, dy, idx):
        print("Making box-conforming mesh with dx = {}".format(dx))
        subprocess.run([
            "python3", "grids/cubicMesh.py",
            "-dx", str(dx), "-dy", str(dy),
            "-o", "{}_{}.dgf".format(meshFilePrefix, idx)
        ], check=True)

    for refIdx in range(numRefinements+1):
        dx = 0.2/pow(2, refIdx)
        makeGrid(dx, dx, refIdx)


def compile(exeName):
    subprocess.run(["make", exeName], check=True)


def runConvergenceTest(exeName, outputName, numRefinements=5, extraArgs=[]):
    if not os.path.exists("convergencetest.py"):
        exitWithError("This is expected to be run from convergence folder")
    baseCall = [
        "python3", "convergencetest.py", exeName, "params.input",
        "-Problem.NumRefinements", str(numRefinements),
        "-Vtk.OutputName", str(outputName)
    ]
    subprocess.run(baseCall + extraArgs, check=True)


def runConvergenceTestAndMoveToFolder(
    exeName, outputName, numRefinements=5, extraArgs=[]
):
    folderName = outputName
    os.makedirs(folderName, exist_ok=True)
    runConvergenceTest(exeName, outputName, numRefinements, extraArgs)
    moveResultsToSubFolder(folderName)


if __name__ == "__main__":
    parser = ArgumentParser(
        description="Result reproduction script for the convergence test."
    )
    parser.add_argument("-n", "--num-refinements", required=False, default=5)
    args = vars(parser.parse_args())

    numRefinements = int(args["num_refinements"])
    exeBox = "test_stokes1p_darcy1pbox"
    exeBoxWeighted = "test_stokes1p_darcy1pbox_weighteddof"
    cubicMeshPrefix = "grids/conforming"

    compile(exeBox)
    compile(exeBoxWeighted)
    prepareCubicMeshes(numRefinements, cubicMeshPrefix)

    removePreviousResults()
    runConvergenceTestAndMoveToFolder(
        exeBox, "box_struct_l2", numRefinements
    )
    runConvergenceTestAndMoveToFolder(
        exeBoxWeighted, "box_struct_weighteddof", numRefinements
    )
    runConvergenceTestAndMoveToFolder(
        exeBox, "box_simplices_l2", numRefinements,
        extraArgs=["-Darcy.Grid.File", "./grids/simplices.msh"]
    )
    runConvergenceTestAndMoveToFolder(
        exeBox, "box_conforming", numRefinements,
        extraArgs=["-Darcy.Grid.FilePrefix", "{}".format(cubicMeshPrefix)]
    )
    runConvergenceTestAndMoveToFolder(
        exeBoxWeighted, "box_simplices_weighteddof", numRefinements,
        extraArgs=["-Darcy.Grid.File", "./grids/simplices.msh"]
    )
