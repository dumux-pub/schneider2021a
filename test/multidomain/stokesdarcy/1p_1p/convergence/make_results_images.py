#!/usr/bin/env pvbatch

import os
from paraview.simple import (
    LoadState,
    GetActiveView,
    GetSources,
    FindSource,
    SaveScreenshot,
    ResetSession
)


def getDarcySourceName(sources):
    return next(s[0] for s in sources if "darcy" in s[0])


def getStokesSourceName(sources):
    return next(s[0] for s in sources if "freeFlow" in s[0])


def makeSnapshot(stateFile, darcyVTKFile, stokesVTKFile, imgFile):
    LoadState(stateFile)

    FindSource(getDarcySourceName(GetSources())).FileName = darcyVTKFile
    FindSource(getStokesSourceName(GetSources())).FileName = stokesVTKFile

    size = (625, 1372)
    view = GetActiveView()
    view.GetRenderWindow().SetSize(size)

    SaveScreenshot(imgFile, view,
                   ImageResolution=(size[0]*2, size[1]*2),
                   TransparentBackground=True)
    ResetSession()


def makeSnapShotFromSubFolder(stateFile, subFolder, imgFile):
    print(f"Making snapshot with state {stateFile} in {subFolder}")
    darcyFile = os.path.join(subFolder, f"{subFolder}_darcy.pvd")
    stokesFile = os.path.join(subFolder, f"{subFolder}_freeFlow.pvd")
    makeSnapshot(stateFile, darcyFile, stokesFile, imgFile)


if __name__ == "__main__":

    state_p = "state_pressure.pvsm"
    state_vx = "state_vx.pvsm"
    state_vy = "state_vy.pvsm"

    # this expects run_paper_simulations.py to have been run before
    makeSnapShotFromSubFolder(state_p, "box_struct_l2", "struct_p.png")
    makeSnapShotFromSubFolder(state_vx, "box_simplices_l2", "simplices_vx.png")
    makeSnapShotFromSubFolder(state_vy, "box_conforming", "conforming_vy.png")
