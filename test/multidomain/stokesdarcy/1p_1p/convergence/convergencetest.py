#!/usr/bin/env python3

from statistics import mean
from math import log, isnan, isinf
import subprocess
import sys


def collectErrors(logFileName, suffix):
    result = {}
    with open(logFileName, 'r') as logFile:
        for line in logFile:
            line = line.strip(r'[ConvergenceTest]')
            errors = line.split('L2(')
            errors = filter(lambda e: '=' in e, errors)
            errors = [err.split('=') for err in errors]
            for name, val in errors:
                name = name.strip().strip(')')
                name = name + "_" + suffix
                val = val.strip()
                if name not in result:
                    result[name] = []
                result[name].append(float(val))
    return result


def collectRates(errors):
    rates = {}
    for name, errValues in errors.items():
        key = name + '_rate'
        if len(errValues) == 1:
            rates[key] = [0.0]
        else:
            for i in range(len(errValues)-1):
                if isnan(errValues[i]) or isinf(errValues[i]):
                    continue
                if not (
                    (errValues[i] < 1e-12 or errValues[i+1] < 1e-12) and
                    (errValues[i] < 1e-12 or errValues[i+1] < 1e-12) and
                    (errValues[i] < 1e-12 or errValues[i+1] < 1e-12)
                ):
                    rate = (log(errValues[i]) - log(errValues[i+1]))/log(2)
                    if key not in rates:
                        rates[key] = []
                    rates[key].append(rate)
                else:
                    raise Exception("Error: exact solution!?")
    return rates


class LatexTableWriter:
    def __init__(self):
        self.data = {}

    def indent_(self, text, level=4):
        return ' '*level + text

    def numCols_(self):
        return len(self.data)

    def numRows_(self):
        return max(len(r) for r in self.data.values())

    def getHeader_(self):
        head = r"\begin{table}[h]" + '\n'
        head += self.indent_(r"\centering", 2) + '\n'
        head += self.indent_(r"\caption{\textbf{Convergence study}.TODO.}", 2) + '\n'
        head += self.indent_(r"\label{tab:todo}", 2) + '\n'
        head += self.indent_(r"\begin{tabular}{", 2)
        head += r'*{' + str(self.numCols_()) + r'}{l}' + '}\n'
        head += self.indent_(r'\toprule')
        return head

    def getDataSection_(self):
        dataSection = ""
        numRows = self.numRows_()
        numCols = self.numCols_()

        for idx, header in enumerate(self.data):
            section = f' {header}'
            if idx < len(self.data) - 1:
                section += ' &'
            dataSection += self.indent_(section) if idx == 0 else section
        dataSection += r" \\" + "\n" + self.indent_(r'\midrule') + '\n'

        for rowIdx in range(numRows):
            for colIdx, data in enumerate(self.data.values()):
                diffToColSize = numRows - len(data)
                rowDataIdx = rowIdx - diffToColSize
                hasData = rowDataIdx >= 0
                rowData = '{:.2e}'.format(data[rowDataIdx]) if hasData else '-'
                section = rowData
                if colIdx < numCols - 1:
                    section += ' & '
                doIndent = colIdx == 0
                dataSection += self.indent_(section) if doIndent else section
            dataSection += r" \\"
            if rowIdx < numRows - 1:
                dataSection += '\n'
        return dataSection

    def getFooter_(self):
        footer = self.indent_(r"\bottomrule", 2) + '\n'
        footer += self.indent_(r"\end{tabular}", 2) + '\n'
        footer += r"\end{table}"
        return footer

    def add(self, dict, mapping):
        for key in mapping:
            if key not in dict:
                raise Exception(f"Key {key} is not in data")
            header = mapping[key]
            self.data[header] = dict[key]

    def write(self, file):
        table = self.getHeader_()
        table += '\n'
        table += self.getDataSection_()
        table += '\n'
        table += self.getFooter_()
        tableFile.write(table)


def checkRate(rateDict, rateKey, expected):
    meanRate = mean(rate for rate in rateDict[rateKey])
    if meanRate < expected:
        raise Exception("Mean rate for {} too low: {}"
                        .format(rateKey, meanRate))

if __name__ == "__main__":
    if len(sys.argv) < 2:
        sys.stderr.write('Please provide the argument <testName> to the script\n')
        sys.exit(1)

    # parse runtime arguments
    darcySuffix = 'darcy'
    freeFlowSuffix = 'freeFlow'
    executableName = str(sys.argv[1])
    testargs = [str(i) for i in sys.argv][2:]

    testName = executableName
    if '-Vtk.OutputName' in testargs:
        testName = testargs[testargs.index('-Vtk.OutputName')+1]
    else:
        testargs.extend(['-Vtk.OutputName', testName])

    numRefinements = 3
    if '-Problem.NumRefinements' in testargs:
        numRefinements = int(testargs[testargs.index('-Problem.NumRefinements')+1])

    gridFilePrefix = ""
    if '-Darcy.Grid.FilePrefix' in testargs:
        gridFilePrefix = testargs[testargs.index('-Darcy.Grid.FilePrefix')+1]

    expectedMeanPressureRate = 1.8
    if '-Problem.ExpectedMeanPressureRate' in testargs:
        expectedMeanPressureRate = testargs[testargs.index('-Problem.ExpectedMeanPressureRate')+1]

    expectedMeanVelocityRate = 1.8
    if '-Problem.ExpectedMeanVelocityRate' in testargs:
        expectedMeanVelocityRate = testargs[testargs.index('-Problem.ExpectedMeanVelocityRate')+1]

    # helper stuff
    def getLogFileName(suffix):
        return testName + '_' + suffix + '.log'

    def removeLogFile(suffix):
        subprocess.call(['rm', getLogFileName(suffix)])
        print("Removed old log file ({})!".format(getLogFileName(suffix)))

    def buildApplication():
        subprocess.run(['make', executableName], check=True)

    def runApplication(refIdx):
        refStr = str(refIdx)
        if gridFilePrefix == "":
            subprocess.call(
                ['./' + executableName] + testargs + ['-Grid.Refinement', refStr]
            )
        else:
            subprocess.call(
                ['./' + executableName] + testargs +
                ['-Stokes.Grid.Refinement', refStr] +
                ['-Darcy.Grid.File', gridFilePrefix + '_' + refStr + '.dgf']
            )

    removeLogFile(darcySuffix)
    removeLogFile(freeFlowSuffix)

    buildApplication()
    for i in range(0, numRefinements+1):
        runApplication(i)

    resultsDarcy = collectErrors(getLogFileName(darcySuffix), darcySuffix)
    resultsDarcy = {**resultsDarcy, **collectRates(resultsDarcy)}
    resultsFreeFlow = collectErrors(getLogFileName(freeFlowSuffix), freeFlowSuffix)
    resultsFreeFlow = {**resultsFreeFlow, **collectRates(resultsFreeFlow)}

    if numRefinements > 0:
        checkRate(resultsDarcy, f'p_{darcySuffix}_rate', expectedMeanPressureRate)
        checkRate(resultsFreeFlow, f'p_{freeFlowSuffix}_rate', expectedMeanPressureRate)
        checkRate(resultsFreeFlow, f'vx_{freeFlowSuffix}_rate', expectedMeanVelocityRate)
        checkRate(resultsFreeFlow, f'vy_{freeFlowSuffix}_rate', expectedMeanVelocityRate)

        writer = LatexTableWriter()
        writer.add(
            resultsFreeFlow,
            {f'p_{freeFlowSuffix}': r'$e_p^m$',
             f'p_{freeFlowSuffix}_rate': r'$r_p$',
             f'vx_{freeFlowSuffix}': r'$e_{v_x}^m$',
             f'vx_{freeFlowSuffix}_rate': r'$r_{v_x}$',
             f'vy_{freeFlowSuffix}': r'$e_{v_y}^m$',
             f'vy_{freeFlowSuffix}_rate': r'$r_{v_y}$'}
        )
        writer.add(
            resultsDarcy,
            {f'p_{darcySuffix}': r'$e_{p^\mathrm{pm}}^m$',
             f'p_{darcySuffix}_rate': r'$r_{p^\mathrm{pm}}$'}
        )
        with open('table.tex', 'w') as tableFile:
            writer.write(tableFile)
