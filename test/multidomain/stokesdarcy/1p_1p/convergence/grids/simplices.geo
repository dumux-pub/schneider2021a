xmin = 0.0;
ymin = 0.0;
xmax = 1.0;
ymax = 1.0;

lx = xmax-xmin;
ly = ymax-ymin;

dx = 1.0/5;
dy = 1.0/5;

Point(1) = {xmin, ymin, 0.0, dx};
Point(2) = {xmin, ymax, 0.0, dx};

Point(3) = {xmax, ymax, 0.0, dx};
Point(4) = {xmax, ymin, 0.0, dx};

Line(1) = {1, 2};
Line(2) = {2, 3};
Line(3) = {3, 4};
Line(4) = {4, 1};

Field[1] = Attractor;
Field[1].NodesList = {2,3};

Field[2] = Threshold;
Field[2].IField = 1;
Field[2].LcMin = 0.7*dx;
Field[2].LcMax = dx;
Field[2].DistMin = 0.1;
Field[2].DistMax = 0.3;

// Use minimum of all the fields as the background field
Field[3] = Min;
Field[3].FieldsList = {2};
Background Field = 3;

Line Loop(1) = {1, 2, 3, 4};

Plane Surface(1) = {1};



