import argparse
import random
parser = argparse.ArgumentParser()
parser.add_argument('-dx', '--deltaX', help='dx', required=True)
parser.add_argument('-dy', '--deltaY', help='dy', required=True)
parser.add_argument('-o', '--output', help='The output grid file', required=True)
args = vars(parser.parse_args())

dx = float(args["deltaX"])
dy = float(args["deltaY"])
outputname = args["output"]
scaling = 0.0

outputfile = open(outputname, 'w')

# parse input file
vertices = []
volumesCube = []

numEleX = int(1.0/dx + 1)
numEleY = int(1.0/dy)

numDofX = numEleX + 1
numDofY = numEleY + 1


print("Deltax" + str(dx))

for i in range(0, numDofY):
  for j in range(0, numDofX):
    if j==0:
      v = [0, dy*i]
    elif j==numDofX-1:
      v = [1.0, dy*i]
    else:
      v = [dx*(j-1) + dx/2, dy*i]
    vertices.append(v)

print("numDofX: " + str(numDofX))
print("numEleY: " + str(numEleY))
print("numEleX: " + str(numEleX))

for i in range(0, numEleY):
  for j in range(0, numEleX):
    #Add cubes to grid
    offsetOne = i*numDofX
    offsetTwo = offsetOne + numDofX
    vol = [offsetOne + j, offsetOne+1 + j, offsetTwo + j, offsetTwo+1 + j]
    volumesCube.append(vol)


# write DGF file
outputfile.write("DGF\n")
outputfile.write("Vertex\n")
for vertex in vertices:
    for coord in vertex:
        outputfile.write(str(coord) + " ")
    outputfile.write("\n")
outputfile.write("#\n")
outputfile.write("CUBE\n")
#outputfile.write("map 0 1 3 2\n")
#outputfile.write("map 0 1 3 2 4 5 7 6\n")
for vol in volumesCube:
    for gvertex in vol:
        outputfile.write(str(gvertex) + " ")
    outputfile.write("\n")
outputfile.write("#\n")
#outputfile.write("BOUNDARYDOMAIN\ndefault 1\n#")
