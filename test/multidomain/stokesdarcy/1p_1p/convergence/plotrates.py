#!/usr/bin/env python3

import os
import sys

try:
    commonPath = os.path.abspath(__file__)
    commonPath = commonPath.rsplit("build-cmake")[0]
    commonPath = os.path.join(commonPath, "test")
    commonPath = os.path.join(commonPath, "common")
    sys.path.append(commonPath)
    from mplconfig import plt
except Exception:
    sys.exit("Could not import mplconfig")

from convergencetest import collectErrors


DARCY_SUFFIX = "darcy"
STOKES_SUFFIX = "freeFlow"


def initializePlot():
    plt.figure()
    plt.clf()


def plotResultsFromSubFolder(subFolder, suffix, key, label):
    logFile = os.path.join(subFolder, f"{subFolder}_{suffix}.log")
    errors = collectErrors(logFile, suffix)
    values = errors[f"{key}_{suffix}"]
    xAxis = list(range(len(values)))
    plt.semilogy(xAxis, values, label=label, marker='o')
    plt.legend()
    plt.grid(True)


def plotReferenceCurve(refSubFolder, suffix, key):
    logFile = os.path.join(refSubFolder, f"{refSubFolder}_{suffix}.log")
    errors = collectErrors(logFile, suffix)
    values = errors[f"{key}_{suffix}"]
    referenceValue = values[0]/2.0
    xAxis = list(range(len(values)))
    plt.semilogy(xAxis,
                 [referenceValue/pow(4, i) for i in range(len(values))],
                 marker="*",
                 linestyle="--",
                 label="$\mathcal{O}(h^2)$")
    plt.legend()


def setAxesLabels(xlabel, ylabel):
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)


def savePlot(name):
    plt.savefig(name, bbox_inches="tight")


if __name__ == "__main__":

    # this expects run_paper_simulations.py to have been run before
    initializePlot()
    plotResultsFromSubFolder("box_struct_l2", DARCY_SUFFIX, "p", "$\Pi^{L2}$ (conforming)")
    plotResultsFromSubFolder("box_struct_weighteddof", DARCY_SUFFIX, "p", "$\Pi^{\Gamma}$ (conforming)")
    plotResultsFromSubFolder("box_simplices_l2", DARCY_SUFFIX, "p", "$\Pi^{L2}$ (non-conforming)")
    plotResultsFromSubFolder("box_simplices_weighteddof", DARCY_SUFFIX, "p", "$\Pi^{\Gamma}$ (non-conforming)")
    plotResultsFromSubFolder("box_conforming", DARCY_SUFFIX, "p", "$\Pi^{L2}$ (box-conforming)")
    plotReferenceCurve(refSubFolder="box_struct_l2", suffix=DARCY_SUFFIX, key="p")
    setAxesLabels("refinement", "$e_{p^\mathrm{pm}}$")
    savePlot("darcy_p.pdf")

    initializePlot()
    plotResultsFromSubFolder("box_struct_l2", STOKES_SUFFIX, "p", "$\Pi^{L2}$ (conforming)")
    plotResultsFromSubFolder("box_struct_weighteddof", STOKES_SUFFIX, "p", "$\Pi^{\Gamma}$ (conforming)")
    plotResultsFromSubFolder("box_simplices_l2", STOKES_SUFFIX, "p", "$\Pi^{L2}$ (non-conforming)")
    plotResultsFromSubFolder("box_simplices_weighteddof", STOKES_SUFFIX, "p", "$\Pi^{\Gamma}$ (non-conforming)")
    plotResultsFromSubFolder("box_conforming", STOKES_SUFFIX, "p", "$\Pi^{L2}$ (box-conforming)")
    plotReferenceCurve(refSubFolder="box_struct_l2", suffix=STOKES_SUFFIX, key="p")
    setAxesLabels("refinement", "$e_{p^\mathrm{ff}}$")
    savePlot("stokes_p.pdf")

    initializePlot()
    plotResultsFromSubFolder("box_struct_l2", STOKES_SUFFIX, "vx", "$\Pi^{L2}$ (conforming)")
    plotResultsFromSubFolder("box_struct_weighteddof", STOKES_SUFFIX, "vx", "$\Pi^{\Gamma}$ (conforming)")
    plotResultsFromSubFolder("box_simplices_l2", STOKES_SUFFIX, "vx", "$\Pi^{L2}$ (non-conforming)")
    plotResultsFromSubFolder("box_simplices_weighteddof", STOKES_SUFFIX, "vx", "$\Pi^{\Gamma}$ (non-conforming)")
    plotResultsFromSubFolder("box_conforming", STOKES_SUFFIX, "vx", "$\Pi^{L2}$ (box-conforming)")
    plotReferenceCurve(refSubFolder="box_struct_l2", suffix=STOKES_SUFFIX, key="vx")
    setAxesLabels("refinement", "$e_{v_x}}$")
    savePlot("stokes_vx.pdf")

    initializePlot()
    plotResultsFromSubFolder("box_struct_l2", STOKES_SUFFIX, "vy", "$\Pi^{L2}$ (conforming)")
    plotResultsFromSubFolder("box_struct_weighteddof", STOKES_SUFFIX, "vy", "$\Pi^{\Gamma}$ (conforming)")
    plotResultsFromSubFolder("box_simplices_l2", STOKES_SUFFIX, "vy", "$\Pi^{L2}$ (non-conforming)")
    plotResultsFromSubFolder("box_simplices_weighteddof", STOKES_SUFFIX, "vy", "$\Pi^{\Gamma}$ (non-conforming)")
    plotResultsFromSubFolder("box_conforming", STOKES_SUFFIX, "vy", "$\Pi^{L2}$ (box-conforming)")
    plotReferenceCurve(refSubFolder="box_struct_l2", suffix=STOKES_SUFFIX, key="vy")
    setAxesLabels("refinement", "$e_{v_y}}$")
    savePlot("stokes_vy.pdf")
