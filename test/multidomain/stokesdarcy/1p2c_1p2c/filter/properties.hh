// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief the stokes and darcy properties files for the coupled channel simulation
 */

#ifndef DUMUX_COUPLED_CHANNEL_PROPERTIES_HH
#define DUMUX_COUPLED_CHANNEL_PROPERTIES_HH

// Both Domains
#include <dune/grid/yaspgrid.hh>
#include <dune/common/hybridutilities.hh>

#include <dumux/multidomain/staggeredtraits.hh>
#include <dumux/multidomain/boundary/stokesdarcy/couplingmanager.hh>

#include <dumux/material/fluidmatrixinteractions/diffusivityconstanttortuosity.hh>
#include <dumux/material/fluidsystems/1padapter.hh>
#include <dumux/material/fluidsystems/h2oair.hh>

// Free-flow domain
#include <dumux/discretization/staggered/freeflow/properties.hh>
#include <dumux/freeflow/compositional/navierstokesncmodel.hh>
#include <dumux/freeflow/navierstokes/boundarytypes.hh>

// Porous medium flow domain
#include <dumux/discretization/box.hh>
#include <dumux/discretization/cctpfa.hh>
#include <dumux/porousmediumflow/1pnc/model.hh>
#include <dumux/discretization/box.hh>

#include "spatialparams.hh"
#include "problem_stokes.hh"
#include "problem_darcy.hh"

#ifndef DARCYDISCMETHODTYPETAG
#define DARCYDISCMETHODTYPETAG BoxModel
#endif

namespace Dumux::Properties {

template<class Traits>
struct FilterTestTraits : public Traits
{
    static constexpr auto projectionMethod()
    {return Dumux::Detail::ProjectionMethod::AreaWeightedDofEvaluation;}
};

namespace TTag {
struct NavierStokesDomain { using InheritsFrom = std::tuple<NavierStokesNC, StaggeredFreeFlowModel>; };
struct DarcyDomain { using InheritsFrom = std::tuple<OnePNC, DARCYDISCMETHODTYPETAG>; };
} // end namespace TTag

// The coupling managers
template<class TypeTag>
struct CouplingManager<TypeTag, TTag::NavierStokesDomain>
{
    using Traits = FilterTestTraits< StaggeredMultiDomainTraits<TypeTag, TypeTag, Properties::TTag::DarcyDomain > >;
    using type = Dumux::StokesDarcyCouplingManager<Traits>;
};

// Set the problem properties
template<class TypeTag>
struct Problem<TypeTag, TTag::NavierStokesDomain> { using type = Dumux::StokesSubProblem<TypeTag>; };

// the fluid system
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::NavierStokesDomain>
{
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using H2OAir = FluidSystems::H2OAir<Scalar>;
    static constexpr auto phaseIdx = H2OAir::gasPhaseIdx;
    using type = FluidSystems::OnePAdapter<H2OAir, phaseIdx>;
};

// Set the grid type
template<class TypeTag>
struct Grid<TypeTag, TTag::NavierStokesDomain>
{
    static constexpr auto dim = 3;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using TensorGrid = Dune::YaspGrid<dim, Dune::TensorProductCoordinates<Scalar, dim> >;
    using HostGrid = TensorGrid;
    using type = Dune::SubGrid<dim, HostGrid>;
};


// Use moles
template<class TypeTag>
struct UseMoles<TypeTag, TTag::NavierStokesDomain> { static constexpr bool value = true; };

// Do not replace one equation with a total mass balance
template<class TypeTag>
struct ReplaceCompEqIdx<TypeTag, TTag::NavierStokesDomain> { static constexpr int value = 3; };

// Set the upwind scheme order for the velocity calculation
template<class TypeTag>
struct UpwindSchemeOrder<TypeTag, TTag::NavierStokesDomain> { static constexpr int value = 1; };

template<class TypeTag>
struct EnableGridGeometryCache<TypeTag, TTag::NavierStokesDomain> { static constexpr bool value = true; };
template<class TypeTag>
struct EnableGridFluxVariablesCache<TypeTag, TTag::NavierStokesDomain> { static constexpr bool value = true; };
template<class TypeTag>
struct EnableGridVolumeVariablesCache<TypeTag, TTag::NavierStokesDomain> { static constexpr bool value = true; };


template<class TypeTag>
struct EnableGridGeometryCache<TypeTag, TTag::DarcyDomain> { static constexpr bool value = true; };
template<class TypeTag>
struct EnableGridFluxVariablesCache<TypeTag, TTag::DarcyDomain> { static constexpr bool value = true; };
template<class TypeTag>
struct EnableGridVolumeVariablesCache<TypeTag, TTag::DarcyDomain> { static constexpr bool value = true; };

// The coupling managers
template<class TypeTag>
struct CouplingManager<TypeTag, TTag::DarcyDomain>
{
    using Traits = FilterTestTraits< StaggeredMultiDomainTraits<Properties::TTag::NavierStokesDomain, Properties::TTag::NavierStokesDomain, TypeTag>>;
    using type = Dumux::StokesDarcyCouplingManager<Traits>;
};

// Set the problem properties
template<class TypeTag>
struct Problem<TypeTag, TTag::DarcyDomain> { using type = Dumux::DarcySubProblem<TypeTag>; };

// the fluid system
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::DarcyDomain>
{
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using H2OAir = FluidSystems::H2OAir<Scalar>;
    static constexpr auto phaseIdx = H2OAir::gasPhaseIdx;
    using type = FluidSystems::OnePAdapter<H2OAir, phaseIdx>;};

// Set the grid type
#if !USEUNSTRUCTUREDGRID
template<class TypeTag>
struct Grid<TypeTag, TTag::DarcyDomain>
{
    static constexpr auto dim = 3;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using TensorGrid = Dune::YaspGrid<dim, Dune::TensorProductCoordinates<Scalar, dim> >;
    using HostGrid = TensorGrid;
    using type = Dune::SubGrid<dim, HostGrid>;
};
#else
template<class TypeTag>
struct Grid<TypeTag, TTag::DarcyDomain> { using type = Dune::UGGrid<3>; };
#endif


// Use moles
template<class TypeTag>
struct UseMoles<TypeTag, TTag::DarcyDomain> { static constexpr bool value = true; };

// Do not replace one equation with a total mass balance
template<class TypeTag>
struct ReplaceCompEqIdx<TypeTag, TTag::DarcyDomain> { static constexpr int value = 3; };

//! Use a model with constant tortuosity for the effective diffusivity
template<class TypeTag>
struct EffectiveDiffusivityModel<TypeTag, TTag::DarcyDomain>
{ using type = DiffusivityConstantTortuosity<GetPropType<TypeTag, Properties::Scalar>>; };

template<class TypeTag>
struct SpatialParams<TypeTag, TTag::DarcyDomain>
{
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using type = DarcySpatialParams<GridGeometry, Scalar>;
};

}
#endif
