SetFactory("OpenCASCADE");

// channel dimensions
scale_factor = 1.0;
channel_length = 1.0*scale_factor;
channel_height = 0.2*scale_factor;
channel_width = 0.2*scale_factor;

// filter dimensions
filter_length = 0.2*scale_factor;
filter_height = 0.2*scale_factor;
filter_curvature_control_point_distance = filter_length*0.2;

// discretization length settings
channel_target_dx = channel_length/100;
channel_target_dy = channel_width/20;
channel_target_dz = channel_height/20;

// top filter
top_filter_bottom = channel_height + filter_height;
top_filter_top = top_filter_bottom + filter_height;
top_filter_if_begin = channel_length - filter_length;

p1 = newp; Point(p1) = {0.0, 0.0, top_filter_bottom};
p2 = newp; Point(p2) = {top_filter_if_begin, 0.0, top_filter_bottom};
p2_dupl = newp; Point(p2_dupl) = {top_filter_if_begin, 0.0, top_filter_bottom};
p3 = newp; Point(p3) = {channel_length, 0.0, top_filter_bottom};

l1 = newl; Line(l1) = {p1, p2};
l2 = newl; Line(l2) = {p2_dupl, p3};

num_cells_y = Round(channel_width/channel_target_dy);
num_cells_z = Round(channel_height/channel_target_dz);

// top channel
out[] = Extrude {0,channel_width,0} { Line{l1, l2}; };
out[] = Extrude {0,0,channel_height} { Surface{:}; };

// create bottom channel by translation
Translate {0, 0, -filter_height-channel_height} { Duplicata { Volume{2}; } }
Translate {channel_length, 0, -filter_height-channel_height} { Duplicata { Volume{1}; } }

/////////////////
// filter volume

// vertical splines
p1_1 = newp; Point(p1_1) = {channel_length - filter_length, 0.0, top_filter_bottom};
p1_2 = newp; Point(p1_2) = {channel_length - filter_length, 0.0, top_filter_bottom - filter_height};
cp_1 = newp; Point(cp_1) = {
    channel_length - filter_length + filter_curvature_control_point_distance,
    filter_curvature_control_point_distance,
    channel_height + filter_height/2.0
};
sp_1 = newl; BSpline(sp_1) = {p1_1, cp_1, p1_2};

p2_1 = newp; Point(p2_1) = {channel_length, 0.0, top_filter_bottom};
p2_2 = newp; Point(p2_2) = {channel_length, 0.0, top_filter_bottom - filter_height};
cp_2 = newp; Point(cp_2) = {
    channel_length - filter_curvature_control_point_distance,
    filter_curvature_control_point_distance,
    channel_height + filter_height/2.0
};
sp_2 = newl; BSpline(sp_2) = {p2_1, cp_2, p2_2};

p3_1 = newp; Point(p3_1) = {channel_length - filter_length, channel_width, top_filter_bottom};
p3_2 = newp; Point(p3_2) = {channel_length - filter_length, channel_width, top_filter_bottom - filter_height};
cp_3 = newp; Point(cp_3) = {
    channel_length - filter_length + filter_curvature_control_point_distance,
    channel_width - filter_curvature_control_point_distance,
    channel_height + filter_height/2.0
};
sp_3 = newl; BSpline(sp_3) = {p3_1, cp_3, p3_2};

p4_1 = newp; Point(p4_1) = {channel_length, channel_width, top_filter_bottom};
p4_2 = newp; Point(p4_2) = {channel_length, channel_width, top_filter_bottom - filter_height};
cp_4 = newp; Point(cp_4) = {
    channel_length - filter_curvature_control_point_distance,
    channel_width - filter_curvature_control_point_distance,
    channel_height + filter_height/2.0
};
sp_4 = newl; BSpline(sp_4) = {p4_1, cp_4, p4_2};

top_edge_1 = newl; Line(top_edge_1) = {p1_1, p2_1};
top_edge_2 = newl; Line(top_edge_2) = {p2_1, p4_1};
top_edge_3 = newl; Line(top_edge_3) = {p4_1, p3_1};
top_edge_4 = newl; Line(top_edge_4) = {p3_1, p1_1};

bottom_edge_1 = newl; Line(bottom_edge_1) = {p1_2, p2_2};
bottom_edge_2 = newl; Line(bottom_edge_2) = {p2_2, p4_2};
bottom_edge_3 = newl; Line(bottom_edge_3) = {p4_2, p3_2};
bottom_edge_4 = newl; Line(bottom_edge_4) = {p3_2, p1_2};

cl = newll; Curve Loop(cl) = {sp_1, bottom_edge_1, -sp_2, -top_edge_1};
front = news; Surface(front) = {cl};

cl = newll; Curve Loop(cl) = {sp_2, bottom_edge_2, -sp_4, -top_edge_2};
right = news; Surface(right) = {cl};

cl = newll; Curve Loop(cl) = {sp_4, bottom_edge_3, -sp_3, -top_edge_3};
back = news; Surface(back) = {cl};

cl = newll; Curve Loop(cl) = {sp_3, bottom_edge_4, -sp_1, -top_edge_4};
left = news; Surface(left) = {cl};

cl = newll; Curve Loop(cl) = {top_edge_1, top_edge_2, top_edge_3, top_edge_4};
top = news; Surface(top) = {cl};

cl = newll; Curve Loop(cl) = {bottom_edge_1, bottom_edge_2, bottom_edge_3, bottom_edge_4};
bottom = news; Surface(bottom) = {cl};

sl = newsl; Surface Loop(sl) = {front, right, back, left, top, bottom};
filter = newv; Volume(filter) = {sl};
Physical Volume(1) = {filter};

// define mesh sizes
DefineConstant[ dx = 0.01 ];
DefineConstant[ dx_interface = 0.1 ];

Field[1] = Distance;
Field[1].SurfacesList = {top, bottom};
Field[1].NumPointsPerCurve = 100;

Field[2] = Threshold;
Field[2].InField = 1;
Field[2].SizeMin = dx;
Field[2].SizeMax = dx_interface;
Field[2].DistMin = filter_height/8.0;
Field[2].DistMax = filter_height/3.0;

Background Field = 2;
