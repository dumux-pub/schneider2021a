#!/bin/bash

runWithErrorCheck() {
    CMD=$1
    if ! $CMD; then
        echo "Command '$CMD' failed"
        exit 1
    fi
}

FETZER_RUN_SCRIPT="../../../../../../docker/run_fetzer2017a.sh"
VTP_EXTRACT_SCRIPT="../../../../../../test/common/extractlinedatavtp.py"
VTU_EXTRACT_SCRIPT="../../../../../../../dumux/bin/postprocessing/extractlinedata.py"
VTK_FILE_NUMBER="00058"
VTP_FILE="test_darcy2p2cnibox_stokes1p2cni_vertical_stokes-face-${VTK_FILE_NUMBER}.vtp"
VTU_FILE="test_darcy2p2cnibox_stokes1p2cni_vertical_stokes-${VTK_FILE_NUMBER}.vtu"

if ! [[ -f $FETZER_RUN_SCRIPT ]]; then
    echo "Could not find fetzer run script"
    exit 1
fi

if ! [[ -f $VTP_EXTRACT_SCRIPT ]]; then
    echo "Could not find vtp data extraction script"
    exit 1
fi

if ! [[ -f $VTU_EXTRACT_SCRIPT ]]; then
    echo "Could not find vtu data extraction script"
    exit 1
fi

echo "Producing results from Fetzer2017a"
runWithErrorCheck $FETZER_RUN_SCRIPT

echo "Producing results with the box scheme"
runWithErrorCheck "./test_darcy2p2cnibox_stokes1p2cni_vertical params_box_ni.input"

echo "Generating csv data"
BOX_CSV_FILE="plot_data_box"
BOX_CSV_TEMPERATURE_FILE="plot_data_box_temperature"
if ! [[ -f $VTP_FILE ]]; then
    echo "Could not find vtp file"
    exit 1
fi

if ! [[ -f $VTU_FILE ]]; then
    echo "Could not find vtu file"
    exit 1
fi

python3 $VTP_EXTRACT_SCRIPT -f $VTP_FILE \
                            -p1 0.0 1.6 0 \
                            -p2 1.0 1.6 0 \
                            -of $BOX_CSV_FILE

pvbatch $VTU_EXTRACT_SCRIPT -f $VTU_FILE \
                            -p1 0.0 1.6 0 \
                            -p2 1.0 1.6 0 \
                            -of $BOX_CSV_TEMPERATURE_FILE

echo "Plotting results"
if ! [[ -f "${BOX_CSV_FILE}.csv" ]]; then
    echo "Could not find box csv data"
    exit 1
fi

python3 plot_results.py -b "${BOX_CSV_FILE}.csv" \
                        -cm1 "method_1/test/coupling-ff-00013_before.csv" \
                        -cm2 "method_2/test/coupling-ff-00013_before.csv" \
                        -cm3 "method_3/test/coupling-ff-00013_before.csv" \
                        -cm4 "method_4/test/coupling-ff-00013_before.csv" \
                        -o "test_fetzer_vertical_plotoverline.pdf"

python3 plot_results.py -b "${BOX_CSV_FILE}.csv" \
                        -cm1 "method_1/test/coupling-ff-00013_before.csv" \
                        -cm2 "method_2/test/coupling-ff-00013_before.csv" \
                        -cm3 "method_3/test/coupling-ff-00013_before.csv" \
                        -cm4 "method_4/test/coupling-ff-00013_before.csv" \
                        -xlim 0.2 0.4 \
                        -ylim 0.5e-5 3e-5 \
                        -o "test_fetzer_vertical_plotoverline_zoom.pdf"

python3 plot_results.py -b "${BOX_CSV_TEMPERATURE_FILE}.csv" \
                        -cm1 "method_1/test/coupling-ff-00013_before.csv" \
                        -cm2 "method_2/test/coupling-ff-00013_before.csv" \
                        -cm3 "method_3/test/coupling-ff-00013_before.csv" \
                        -cm4 "method_4/test/coupling-ff-00013_before.csv" \
                        -o "test_fetzer_vertical_temperature_plotoverline.pdf" \
                        -f "temperature"

python3 plot_results.py -b "${BOX_CSV_TEMPERATURE_FILE}.csv" \
                        -cm1 "method_1/test/coupling-ff-00013_before.csv" \
                        -cm2 "method_2/test/coupling-ff-00013_before.csv" \
                        -cm3 "method_3/test/coupling-ff-00013_before.csv" \
                        -cm4 "method_4/test/coupling-ff-00013_before.csv" \
                        -xlim 0.2 0.4 \
                        -ylim 293.5 300 \
                        -o "test_fetzer_vertical_temperature_plotoverline_zoom.pdf" \
                        -f "temperature"
