#!/usr/bin/env python3
import argparse
import sys
import os
import numpy as np

try:
    commonPath = os.path.abspath(__file__)
    commonPath = commonPath.rsplit("build-cmake")[0]
    commonPath = os.path.join(commonPath, "test")
    commonPath = os.path.join(commonPath, "common")
    sys.path.append(commonPath)
    from mplconfig import plt
except Exception:
    sys.exit("Could not import mplconfig")

# parse arguments
parser = argparse.ArgumentParser(
  description='Plot the y-velocity from the obtained results.'
)
parser.add_argument('-b', '--box-csv-file',
                    required=True,
                    help="csv file with the data obtained with the box scheme")
parser.add_argument('-cm1', '--fetzer-cm1-csv-file',
                    required=True,
                    help="csv file with the data of fetzer2017a (with cm1)")
parser.add_argument('-cm2', '--fetzer-cm2-csv-file',
                    required=True,
                    help="csv file with the data of fetzer2017a (with cm2)")
parser.add_argument('-cm3', '--fetzer-cm3-csv-file',
                    required=True,
                    help="csv file with the data of fetzer2017a (with cm3)")
parser.add_argument('-cm4', '--fetzer-cm4-csv-file',
                    required=True,
                    help="csv file with the data of fetzer2017a (with cm4)")
parser.add_argument('-xlim', '--x_axis_limit',
                    type=float, nargs=2, default=None,
                    help="two values specifying the range of x-axis")
parser.add_argument('-ylim', '--y_axis_limit',
                    type=float, nargs=2, default=None,
                    help="two values specifying the range of y-axis")
parser.add_argument('-o', '--outputName',
                    type=str, required=True,
                    help="the output name of the file")
parser.add_argument('-f', '--field',
                    type=str, required=False, choices=["vy", "temperature"], default="vy",
                    help="the output name of the file")


args = vars(parser.parse_args())
fieldName = args["field"]
fieldDataMap = {
    "vy": {
        "fetzer_field_name": "velocityN1",
        "box_field_index": 3,
        "field_label": "$v_y$ [m/s]"
    },
    "temperature": {
        "fetzer_field_name": "temperature",
        "box_field_name": "T",
        "field_label": "$T$ [K]"
    }
}

def get_fetzer_field_data(all_fetzer_data):
    return all_fetzer_data[fieldDataMap[fieldName]["fetzer_field_name"]]

def get_box_field_data(all_box_data):
    if fieldName == "vy":
        return all_box_data[:, fieldDataMap[fieldName]["box_field_index"]]
    return all_box_data[fieldDataMap[fieldName]["box_field_name"]]

def get_box_x_values(all_box_data):
    if fieldName == "vy":
        return all_box_data[:, 0]
    return all_box_data["Points0"]

def get_field_label():
    return fieldDataMap[fieldName]["field_label"]


boxUseNames = None if fieldName == "vy" else True
boxData = np.genfromtxt(args["box_csv_file"], delimiter=",", names=boxUseNames)
cm1Data = np.genfromtxt(args["fetzer_cm1_csv_file"], delimiter=",", names=True)
cm2Data = np.genfromtxt(args["fetzer_cm2_csv_file"], delimiter=",", names=True)
cm3Data = np.genfromtxt(args["fetzer_cm3_csv_file"], delimiter=",", names=True)
cm4Data = np.genfromtxt(args["fetzer_cm4_csv_file"], delimiter=",", names=True)

plt.figure()
plt.plot(get_box_x_values(boxData), get_box_field_data(boxData), label="$\Pi^{\mathrm{L2}}$ (box-conforming)", drawstyle="steps-post")
plt.plot(cm1Data["Points0"], get_fetzer_field_data(cm1Data), label="Fetzer2017 (CM1)")
plt.plot(cm2Data["Points0"], get_fetzer_field_data(cm2Data), label="Fetzer2017 (CM2)")
plt.plot(cm3Data["Points0"], get_fetzer_field_data(cm3Data), label="Fetzer2017 (CM3)")
plt.plot(cm4Data["Points0"], get_fetzer_field_data(cm4Data), label="Fetzer2017 (CM4)")
plt.ylabel(get_field_label())
plt.xlabel("x [m]")
plt.legend()
if args["x_axis_limit"] is not None:
    plt.xlim(args["x_axis_limit"])
if args["y_axis_limit"] is not None:
    plt.ylim(args["y_axis_limit"])
plt.savefig(args["outputName"], bbox_inches="tight")
