#!/usr/bin/env pvbatch

import os
from paraview.simple import (
    LoadState,
    Render,
    SaveScreenshot,
    ResetSession
)

def makeSnapshot(stateFile, imgFile):
    LoadState(stateFile)
    Render()
    SaveScreenshot(
    filename=imgFile,
    ImageResolution=(3000, 1342),
    TransparentBackground=1
    )
    ResetSession()

if __name__ == "__main__":

    state_massfraction = "mass_fraction.pvsm"
    state_temperature = "temperature.pvsm"
    state_vnorm_sat = "v_ff_sat_pm.pvsm"

    # this expects produce_and_plot_all_results.sh to have been run before
    makeSnapshot(state_massfraction, "fetzer_mass_frac_final.png")
    makeSnapshot(state_temperature, "fetzer_temperature_final.png")
    makeSnapshot(state_vnorm_sat, "fetzer_v_ff_sat_pm_final.png")
