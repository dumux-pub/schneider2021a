#!/bin/bash

echo "This computes the results for the test case presented in Fetzer2017"
echo "This spins up a docker container, so docker needs to be installed"
echo "Also, this may take several hours to compute..."

runFetzer() {

   CID=$1
   METHOD=$2
   FOLDER=$3

   REFINEMENT=5
   APPL_PATH="dumux-Fetzer2017a/build-gcc/appl/staggeredgrid/multidomain/navierstokes2ctdarcy2p2ct/test"

   mkdir -p $FOLDER
   docker exec $CID /bin/bash -c "cd $APPL_PATH && ./windtunnel coupling.input -Coupling.Method $METHOD -Grid.Refinement $REFINEMENT -Output.PlotVelocityProfiles true"
   docker cp $CID:/fetzer2017a/$APPL_PATH $FOLDER
}

CID=$(docker create -t git.iws.uni-stuttgart.de:4567/dumux-pub/schneider2019b/fetzer2017a)
docker start $CID

runFetzer $CID 4 method_4
runFetzer $CID 1 method_1
runFetzer $CID 2 method_2
runFetzer $CID 3 method_3

docker stop $CID
docker rm $CID
